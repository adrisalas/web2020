const express = require("express");
const router = express.Router();
const userService = require("../services/userService");
const experienceService = require("../services/experienceService");

exports.findAll = async (req, res, next) => {
  try {
    const usersWithExperience = [];
    let users = [];
    if (req.query.name) {
      users = await userService.findByName(req.query.name);
    } else {
      users = await userService.findAll();
    }
    console.log(users);
    for (let u of users) {
      const exp = await experienceService.getExperienceByUserId(u._id);
      usersWithExperience.push({...u._doc, experience: exp});
    }
    res.status(200).json(usersWithExperience);
  }catch (e){
    next(e);
  }
};

exports.findById = async (req, res, next) => {
  try {
    const u = await userService.findById(req.params.id);
    const exp = await experienceService.getExperienceByUserId(u._id);
    res.status(200).json({ ...u._doc, experience: exp });
  } catch (err) {
    next(err);
  }
};

exports.create = async (req, res, next) => {
  try {
    res.status(201).json(await userService.create(req.body));
  } catch (err) {
    next(err);
  }
};

exports.delete = async (req, res, next) => {
  try {
    res.status(200).json(await userService.delete(req.params.id));
  } catch (err) {
    next(err);
  }
};

exports.put = async (req, res, next) => {
  try {
    res.status(200).json(await userService.put(req.params.id, req.body));
  } catch (err) {
    next(err);
  }
};
