const commentService = require("../services/commentService");

exports.findAll = async (req, res, next) => {
  try {
    const graffitiId = req.query.graffiti;
    const routeId = req.query.route;
    let doc;

    if (graffitiId) {
      doc = await commentService.findByGraffiti(graffitiId);
    } else if (routeId) {
      doc = await commentService.findByRoute(routeId);
    } else {
      doc = await commentService.findAll();
    }
    res.status(200).json(doc);
  } catch (err) {
    next(err);
  }
};

exports.create = async (req, res, next) => {
  try {
    if (req.authData._id == req.body.author._id) {
      const doc = await commentService.create(req.body);
      res.status(201).json(doc);
    } else {
      const error = new Error(
        "You are: " +
          req.authData._id +
          " you cannot create comments of user: " +
          req.body.author._id
      );
      error.status = 403;
      throw error;
    }
  } catch (err) {
    next(err);
  }
};

exports.findById = async (req, res, next) => {
  try {
    const doc = await commentService.findById(req.params.id);
    res.status(200).json(doc);
  } catch (err) {
    next(err);
  }
};

exports.delete = async (req, res, next) => {
  try {
    const doc = await commentService.findById(req.params.id);
    if (doc.author._id == req.authData._id) {
      const doc = await commentService.delete(req.params.id);
      res.status(200).json(doc);
    } else {
      const error = new Error(
        "You are: " +
          req.authData._id +
          " you cannot delete comments of user: " +
          doc.author._id
      );
      error.status = 403;
      throw error;
    }
  } catch (err) {
    next(err);
  }
};

exports.put = async (req, res, next) => {
  try {
    if (req.body.author._id === req.authData._id) {
      const doc = await commentService.edit(req.params.id, req.body);
      res.status(200).json(doc);
    } else {
      const error = new Error(
        "You are: " +
          req.authData._id +
          " you cannot edit comments of user: " +
          req.body.author._id
      );
      error.status = 403;
      throw error;
    }
  } catch (err) {
    next(err);
  }
};

exports.addPositiveVote = async (req, res, next) => {
  try {
    const doc = await commentService.addPositiveVote(
      req.params.id,
      req.authData._id
    );
    res.status(201).json(doc);
  } catch (err) {
    next(err);
  }
};

exports.removePositiveVote = async (req, res, next) => {
  try {
    const doc = await commentService.removePositiveVote(
      req.params.id,
      req.authData._id
    );
    res.status(200).json(doc);
  } catch (err) {
    next(err);
  }
};

exports.addNegativeVote = async (req, res, next) => {
  try {
    const doc = await commentService.addNegativeVote(
      req.params.id,
      req.authData._id
    );
    res.status(201).json(doc);
  } catch (err) {
    next(err);
  }
};

exports.remoteNegativeVote = async (req, res, next) => {
  try {
    const doc = await commentService.removeNegativeVote(
      req.params.id,
      req.authData._id
    );
    res.status(200).json(doc);
  } catch (err) {
    next(err);
  }
};
