const weatherService = require("../services/weatherService");

exports.findByCoordinates = async (req, res, next) => {
  try {
    res
      .status(200)
      .json(
        await weatherService.findByCoordinates(req.query.lat, req.query.lon)
      );
  } catch (e) {
    next(e);
  }
};
