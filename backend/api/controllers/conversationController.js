const express = require("express");
const router = express.Router();
const conversationService = require("../services/conversationService");

exports.findAll = async (req, res, next)=>{
    try{
        if(req.query.user1 && req.query.user2){
            //find conversations from logged username
            const doc = await conversationService.findByTwoUsers(req.query.user1, req.query.user2);
            console.log("DOC");
            console.log(doc);
            res.status(200).json(doc);
        }else if(req.query.userId){
            console.log(req.query.userId);
            const doc = await conversationService.findByUserId(req.query.userId);
            res.status(200).json(doc);
        }else if(req.query.user1){
            //find conversations from logged username
            console.log(req.query.user1);
            const doc = await conversationService.findByLoggedUser(req.query.user1);
            res.status(200).json(doc);
        }else{
            const doc = await conversationService.findAll();
            res.status(200).json(doc);
        }
    }catch (e) {
        next(e);
    }
};

exports.findById = async (req, res, next)=>{
    try{
        const doc = await conversationService.findById(req.params.id);
        res.status(200).json(doc);
    }catch (e) {
        next(e);
    }
};

exports.create = async (req, res, next) => {
    try{
        const doc = await conversationService.create(req.body);
        res.status(201).json(doc);
    }catch (e) {
        next(e);
    }
};

exports.delete = async (req,res,next) => {
    try{
        const doc = await conversationService.delete(req.params.id);
        res.status(200).json(doc);
    }catch (e) {
        next(e);
    }
};

exports.edit = async (req,res,next) => {
    try{
        const doc = await conversationService.edit(req.params.id, req.body);
        res.status(200).json(doc);
    }catch (e) {
        next(e);
    }
};

exports.send = async (req, res, next) => {
    try{
        const doc = await conversationService.send(req.params.id, req.body);
        res.status(201).json(doc);
    }catch (e) {
        next(e);
    }
};