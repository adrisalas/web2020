const wifiService = require("../services/wifiService");
const wifiUrl = 'https://datosabiertos.malaga.eu/recursos/urbanismoEInfraestructura/sedesWifi/da_sedesWifi-25830.geojson';
const fontUrl = 'https://datosabiertos.malaga.eu/recursos/ambiente/fuentesaguapotable/da_medioAmbiente_fuentes-4326.geojson';
const parkUrl = 'https://datosabiertos.malaga.eu/recursos/ambiente/parquesCaninos/da_parquesCaninos-4326.geojson';

exports.findAll = async (req, res, next) => {
    try{
        let doc = [];
        if(req.query.latitude && req.query.longitude && req.query.radius){
            doc = await wifiService.wifiPointsOfInterest(
                'https://datosabiertos.malaga.eu/recursos/urbanismoEInfraestructura/sedesWifi/da_sedesWifi-25830.geojson',
                req.query.latitude, req.query.longitude, req.query.radius);
            //console.log(doc);
            res.status(200).json(doc);
        }else{
            doc = await wifiService.findAll('https://datosabiertos.malaga.eu/recursos/urbanismoEInfraestructura/sedesWifi/da_sedesWifi-25830.geojson');
            //console.log(doc);
            res.status(200).json(doc);
        }
    }catch (e) {
        next(e);
    }
};

exports.findAllFonts = async (req, res, next) => {
    try{
        let doc = [];
        if(req.query.latitude && req.query.longitude && req.query.radius){
            doc = await wifiService.fontsOfInterest(fontUrl,
                req.query.latitude, req.query.longitude, req.query.radius);
            //console.log(doc);
            res.status(200).json(doc);
        }else{
            doc = await wifiService.findAllFonts(fontUrl);
            //console.log(doc);
            res.status(200).json(doc);
        }
    }catch (e) {
        next(e);
    }
};

exports.findAllParks = async (req, res, next) => {
    try{
        let doc = [];
        if(req.query.latitude && req.query.longitude && req.query.radius){
            doc = await wifiService.parksOfInterest(parkUrl,
                req.query.latitude, req.query.longitude, req.query.radius);
            //console.log(doc);
            res.status(200).json(doc);
        }else{
            doc = await wifiService.findAllParks(parkUrl);
            //console.log(doc);
            res.status(200).json(doc);
        }
    }catch (e) {
        next(e);
    }
};
