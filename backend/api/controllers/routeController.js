const express = require("express");
const router = express.Router();
const routeService = require("../services/routeService");

exports.findAll = async function (req, res, next) {
  try {
    res.status(200).json(await routeService.findAll());
  } catch (err) {
    next(err);
  }
};

exports.findById = async (req, res, next) => {
  try {
    res.status(200).json(await routeService.findById(req.params.id));
  } catch (err) {
    next(err);
  }
};

exports.create = async (req, res, next) => {
  try {
    res.status(201).json(await routeService.create(req.body));
  } catch (err) {
    next(err);
  }
};

exports.delete = async (req, res, next) => {
  try {
    res.status(200).json(await routeService.delete(req.params.id));
  } catch (err) {
    next(err);
  }
};

exports.deleteAll = async (req, res, next) => {
  try {
    res.status(200).json(await routeService.deleteAll());
  } catch (err) {
    next(err);
  }
};

exports.put = async (req, res, next) => {
  try {
    res.status(200).json(await routeService.put(req.params.id, req.body));
  } catch (err) {
    next(err);
  }
};
exports.addPositiveVote = async (req, res, next) => {
  try {
    res
      .status(200)
      .json(await routeService.addPositiveVote(req.params.id, req.body._id));
  } catch (e) {
    next(e);
  }
};

exports.addNegativeVote = async (req, res, next) => {
  try {
    res
      .status(200)
      .json(await routeService.addNegativeVote(req.params.id, req.body._id));
  } catch (e) {
    next(e);
  }
};
exports.removePositiveVote = async (req, res, next) => {
  try {
    res
      .status(200)
      .json(await routeService.removePositiveVote(req.params.id, req.body._id));
  } catch (e) {
    next(e);
  }
};

exports.removeNegativeVote = async (req, res, next) => {
  try {
    res
      .status(200)
      .json(await routeService.removeNegativeVote(req.params.id, req.body._id));
  } catch (e) {
    next(e);
  }
};

exports.addGraffiti = async (req, res, next) => {
  try {
    res
      .status(200)
      .json(await routeService.addGraffiti(req.params.id, req.body.graffitiId));
  } catch (e) {
    next(e);
  }
};
exports.deleteAllGraffitis = async (req, res, next) => {
  try {
    console.log(req.params);
    res.status(200).json(await routeService.deleteAllGraffitis(req.params.id));
  } catch (err) {
    next(err);
  }
};
