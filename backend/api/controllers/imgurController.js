const imgurService = require("../services/imgurService");

exports.upload = async (req, res, next) => {
  try {
    res.status(201).json(await imgurService.upload(req.body));
  } catch (e) {
    next(e);
  }
};
