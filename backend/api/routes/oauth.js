const express = require('express');
const router = express.Router();
require('dotenv').config();
const jwt = require('jsonwebtoken');
const User = require("../models/user");
const allFacade = require('../dao/AllFacade');
const {OAuth2Client} = require('google-auth-library');
const clientId = process.env.GOOGLE_CLIENT_ID;
const client = new OAuth2Client(clientId);

router
    .route("/")
    .post(async (req, res, next) => {
        try {
            let token = req.body.idtoken;

            const ticket = await client.verifyIdToken({
                idToken: token,
                audience: clientId
            });
            const payload = ticket.getPayload();
            const userid = payload['sub'];

            User.findOne({idGoogle: userid}, async function (err, user) {
                if (!err) {
                    if (!user) { // Creamos el usuario
                        const nuevoUsuario = {
                            email: payload.email,
                            name: payload.name,
                            idGoogle: userid,
                            image: payload.picture,
                            graffitis: [],
                            banner: ""
                            // falta banner, rol, description
                        }
                        user = await allFacade.create(User, nuevoUsuario);
                    }
                    const token = jwt.sign({_id: user._id, rol: user.rol}, process.env.JWT_SECRET);

                    return res.status(200).json({
                        token,
                        user: user
                    });
                } else {
                    let error = new Error(err);
                    error.status = 500;
                    throw error;
                }
            })
        } catch
            (err) {
            next(err);
        }
    })
;
module.exports = router;
