const express = require("express");
const mongoose = require("mongoose");
const router = express.Router();

const Message = require("../models/message");

router.get("/", (req, res, next) => {
  Message.find()
    .exec()
    .then((docs) => {
      res.status(200).json(docs);
    })
    .catch((err) => {
      const error = new Error(err);
      error.status = 500;
      next(error);
    });
});

router.post("/", (req, res, next) => {
  console.log(req.body);
  if (req.body.message) {
    const msg = new Message({
      _id: new mongoose.Types.ObjectId(),
      message: req.body.message,
    });
    msg
      .save()
      .then((doc) => {
        res.status(201).json(doc);
      })
      .catch((err) => {
        const error = new Error(err);
        error.status = 500;
        next(error);
      });
  } else {
    const error = new Error("No message found in body");
    error.status = 400;
    next(error);
  }
});

router.get("/:id", (req, res, next) => {
  const id = req.params.id;
  Message.findById(id)
    .exec()
    .then((doc) => {
      if (doc) {
        res.status(200).json(doc);
      } else {
        const error = new Error("id=" + id + " not found");
        error.status = 404;
        next(error);
      }
    })
    .catch((err) => {
      const error = new Error(err);
      error.status = 500;
      next(error);
    });
});

router.delete("/:id", (req, res, next) => {
  const id = req.params.id;
  Message.remove({ _id: id })
    .exec()
    .then((doc) => {
      res.status(200).json(doc);
    })
    .catch((err) => {
      const error = new Error(err);
      error.status = 500;
      next(error);
    });
});

router.put("/:id", (req, res, next) => {
  const id = req.params.id;
  console.log(req.body);
  Message.update({ _id: id }, { $set: req.body })
    .exec()
    .then((doc) => {
      res.status(200).json(doc);
    })
    .catch((err) => {
      const error = new Error(err);
      error.status = 500;
      next(error);
    });
});

module.exports = router;
