const express = require("express");
const router = express.Router();
const routeController = require("../controllers/routeController");

router.get("/", routeController.findAll);
router.get("/:id", routeController.findById);

router.post("/", routeController.create);

router.delete("/", routeController.deleteAll);
router.delete("/:id", routeController.delete);
router.delete("/:id/votes/positives", routeController.removePositiveVote);
router.delete("/:id/votes/negatives", routeController.removeNegativeVote);
router.delete("/:id/graffitis", routeController.deleteAllGraffitis);

router.put("/:id", routeController.put);
router.post("/:id/votes/negatives", routeController.addNegativeVote);
router.post("/:id/votes/positives", routeController.addPositiveVote);
router.put("/:id/graffitis", routeController.addGraffiti);

module.exports = router;
