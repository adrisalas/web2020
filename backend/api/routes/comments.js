const express = require("express");
const router = express.Router();
const commentController = require("../controllers/commentController");
const jwtService = require("../services/jwtService");

router
  .route("/")
  .get(commentController.findAll)
  .post(jwtService.checkTokenMW, commentController.create);

router
  .route("/:id")
  .get(commentController.findById)
  .delete(jwtService.checkTokenMW, commentController.delete)
  .put(jwtService.checkTokenMW, commentController.put);

router
  .route("/:id/votes/positives")
  .post(jwtService.checkTokenMW, commentController.addPositiveVote)
  .delete(jwtService.checkTokenMW, commentController.removePositiveVote);

router
  .route("/:id/votes/negatives")
  .post(jwtService.checkTokenMW, commentController.addNegativeVote)
  .delete(jwtService.checkTokenMW, commentController.remoteNegativeVote);

module.exports = router;
