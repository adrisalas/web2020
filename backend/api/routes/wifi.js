const express = require("express");
const router = express.Router();
const wifiController = require("../controllers/wifiController");

router.get("/wifi", wifiController.findAll);
router.get("/fuentes", wifiController.findAllFonts);
router.get("/parques", wifiController.findAllParks);
module.exports = router;