const express = require ('express');
const router = express.Router();
const conversationController = require('../controllers/conversationController');

router.get('/', conversationController.findAll);
router.post('/', conversationController.create);
router.delete('/:id', conversationController.delete);
router.get('/:id', conversationController.findById);
router.put('/:id', conversationController.edit);
router.post('/:id/send', conversationController.send);

module.exports = router;
