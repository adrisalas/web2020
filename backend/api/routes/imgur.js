const express = require("express");
const router = express.Router();
const imgurController = require("../controllers/imgurController");

router.post("/", imgurController.upload);

module.exports = router;
