const fetch = require("node-fetch");
const express = require('express');
const router = express.Router();

router.route("/search")
    .get(async (req, res, next) => {
        let ciudad = req.query.ciudad;

        let requestOptions = {
            method: 'GET',
            headers: {"Accept": "application/json"},
            redirect: 'follow'
        };
        let url = "https://nominatim.openstreetmap.org/search?q=" + ciudad +
            "&format=json&addressdetails=1";

        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                const datos = data[0];
                const obj = {
                    lat: datos.lat,
                    lon: datos.lon,
                    address: datos.address
                }
                return res.status(200).json(obj);
            })
            .catch(error => next(error));
    });

router.route("/reverse")
    .get(async (req, res, next) => {
        console.log(req.query);
        let lat = req.query.lat;
        let lon = req.query.lon;
console.log(lat);
        console.log(lon);

        let requestOptions = {
            method: 'GET',
            headers: {"Accept": "application/json"},
            redirect: 'follow'
        };
        let url = "https://nominatim.openstreetmap.org/reverse?lat=" + lat +
            "&lon=" + lon +
            "&format=json&addressdetails=1";

        fetch(url, requestOptions)
            .then(response => response.json())
            .then(data => {
                console.log(data);
                const obj = {
                    display_name: data.display_name,
                    address: data.address
                }
                return res.status(200).json(obj);
            })
            .catch(error => next(error));
    });

module.exports = router;
