const fetch = require("node-fetch");
const express = require('express');
const router = express.Router();

router.route("/")
    .get(async (req, res, next) => {
        let requestOptions = {
            method: 'GET',
            headers: {"Accept": "application/json"},
            redirect: 'follow'
        };

        fetch("https://api.chucknorris.io/jokes/random?category=dev", requestOptions)
            .then(response => response.json())
            .then(data => res.status(200).json({value: data.value}))
            .catch(error => next(error));
    });


module.exports = router;
