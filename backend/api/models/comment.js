const mongoose = require("mongoose");

const commentSchema = mongoose.Schema({
  _id: mongoose.Schema.Types.ObjectId,
  message: String,
  author: {
    _id: mongoose.Schema.Types.ObjectId,
    name: String,
    image: String,
  },
  parent: {
    type: {
      type: String,
      enum: ["Graffiti", "Route"],
    },
    _id: mongoose.Schema.Types.ObjectId,
  },
  graffiti: mongoose.Schema.Types.ObjectId,
  date: Date,
  votes: {
    positives: [
      {
        _id: mongoose.Schema.Types.ObjectId,
      },
    ],
    negatives: [
      {
        _id: mongoose.Schema.Types.ObjectId,
      },
    ],
  },
});

module.exports = mongoose.model("Comment", commentSchema);
