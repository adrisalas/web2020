const mongoose = require('mongoose');
const conversationSchema = mongoose.Schema({
    _id : mongoose.Schema.Types.ObjectId,
    last_updated: Date,
    users : [
        {
            _id : mongoose.Schema.Types.ObjectId,
            nickname : String
        },
        {
            _id : mongoose.Schema.Types.ObjectId,
            nickname : String
        }
    ],
    messages : [
        {
            message : String,
            date : Date,
            user : mongoose.Schema.Types.ObjectId
        }
    ]
});
module.exports = mongoose.model("Conversation", conversationSchema);