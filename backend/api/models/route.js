const mongoose = require("mongoose");
const Schema = require("mongoose");

const subSchemaVote = mongoose.Schema(
    {
        _id: {
            type: Schema.ObjectId,
            required: [true, "The User Id cannot be null"],
        },
    },
    {_id: false}
);

const subSchemaGraffiti = mongoose.Schema(
    {
        graffiti_id: Schema.ObjectId,
        title: String,
        date: Date,
        location: {lon: Number, lat: Number},
        author: {
            author_id: {
                type: mongoose.Schema.ObjectId,
                required: [true, "The User Id cannot be null"],
            },
            name: String
        },
        image: String,
        state: String,
        tags: [String],
    },
    {_id: false}
);

const subSchemaComment = mongoose.Schema(
    {
        comment_id: {
            type: Schema.ObjectId,
            required: [true, "The Comment Id cannot be null"],
        },
        author: {
            author_id: {
                // para acceder al perfil de autor
                type: Schema.ObjectId,
                required: [true, "The Author Id cannot be null"],
            },
            name: {
                type: String,
                required: [true, "The Author Name cannot be blank."],
            },
        },
        message: {
            //para acceder al perfil de autor
            type: String,
            required: [true, "The Message cannot be blank."],
        },
        date: Date,
        votes: {
            positives: [subSchemaVote],
            negatives: [subSchemaVote],
        },
    },
    {_id: false}
);

const schema = mongoose.Schema({
    _id: {type: Schema.ObjectId, auto: true},
    author: {
        author_id: {
            type: Schema.ObjectId,
            required: [true, "The Author Id cannot be null"],
        },
        name: {
            //para acceder al perfil de autor
            type: String,
            required: [true, "The Author Name cannot be blank."],
        },
    },
    title: {
        type: String,
        required: [true, "The title cannot be blank."],
        maxlength: [30, "Route Name Is To Long !"],
    },
    description: {
        type: String,
        required: [true, "The description cannot be blank."],
        maxlength: [500, "Description Name Is To Long !"],
    },
    city: {
        type: String,
        default: "Unknow",
        maxlength: [30, "City Name Is To Long !"],
    },
    location: {lon: Number, lat: Number},
    kilometers: Number, //se calcula con openstreetmap
    date: {
        type: Date,
        required: [true, "The Date cannot be blank."],
    },
    graffitis: [subSchemaGraffiti],
    votes: {
        positives: [subSchemaVote],
        negatives: [subSchemaVote],
    },
    comments: [subSchemaComment],
});

module.exports = mongoose.model("Route", schema);
