const AllFacade = require('../dao/AllFacade');
const mongoose = require('mongoose');

exports.findAll = async (model)=>{
    const doc = model.find()
        .sort({last_updated : -1})
        .exec()
        .catch((err)=>{
            const error = new Error(err);
            error.status = 500;
            throw error;
        });
    return doc;
};

exports.create = async (model, conv) => {
    const conversation = new model({
        _id : new mongoose.Types.ObjectId(),
        last_updated : new Date(),
        users : conv.users,
        messages : []
    });

    return conversation
        .save()
        .catch((err)=>{
            const error = new Error(err);
            error.status = 500;
            throw error;
        });
};

exports.send = async (model,id,msg) => {
    const doc = AllFacade.findById(id); //await ?????
    if(doc){
        let d = new Date();
        const updatedDoc = model.updateOne(
            {_id: id},
            {
                $set : {
                    last_updated : d
                },
                $push:
                    {"messages" :
                            {
                                message : msg.message,
                                date : d,
                                user : msg.user
                            }
                    }
            }
        )
            .exec()
            .catch((err)=>{
                errorHandler(err);;
            });
        return updatedDoc;
    }else{
        const error = new Error("Conversation " + id + " not found");
        error.status = 404;
        throw error;
    }
};

function errorHandler(err) {
    let error = new Error(err);
    switch (err.name) {
        case "ValidationError":
            error.status = 422;
            break;
        case "CastError":
            error.status = 404;
            break;
        default:
            error.status = 500;
    }
    throw error;
}