const {errorHandler} = require("../dao/AllFacade");

exports.addGraffiti = async (model, id, graffiti) => {
    const doc = await model.findById(id);

    if (doc) {
        if (doc.graffitis.some((v) => v.graffiti_id === graffiti._id)) {
            const error = new Error("The route has already this graffiti");
            error.status = 409;
            throw error;
        }

        return model.updateOne(
            {_id: id},
            {
                $push: {
                    "graffitis": {
                        graffiti_id: graffiti._id,
                        title: graffiti.title,
                        date: graffiti.date,
                        location: {lon: graffiti.location.lon, lat: graffiti.location.lat,},
                        image: graffiti.image,
                        state: graffiti.state,
                        tags: graffiti.tags,
                        author: graffiti.author
                    }
                }
            }
        )
            .exec()
            .catch((err) => {
                errorHandler(err);
            });
    } else {
        const error = new Error("id=" + id + " not found");
        error.status = 404;
        throw error;
    }
};



exports.deleteAllGraffitis = async (model, id) => {
    const doc = await model.findById(id);

    if (doc) {
        return model.updateOne( {_id: id},{ $set: { graffitis: [] }})
            .exec()
            .catch((err) => {
                errorHandler(err);
            });
    } else {
        const error = new Error("id=" + id + " not found");
        error.status = 404;
        throw error;
    }
};
