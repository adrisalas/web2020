const fetch = require('node-fetch');
const urlMetres = 'https://datosabiertos.malaga.eu/recursos/ambiente/fuentesaguapotable/da_medioAmbiente_fuentes-25830.geojson';
const urlDegrees = 'https://datosabiertos.malaga.eu/recursos/ambiente/fuentesaguapotable/da_medioAmbiente_fuentes-4326.geojson';

exports.findAll = async (url) => {
    let results = [];
    return fetch(urlDegrees)
        .then(resp => resp.json())
        .then((data) => {
            data.features.map(feature => {
                let coordinates = [];
                if (feature.geometry != null)
                    coordinates = feature.geometry.coordinates;

                const font = {
                    id: feature.id,
                    coordinates: coordinates,
                    nombre: feature.properties.nombre || "no info",
                };
                results.push(font);
            });
            //console.log(results);
            return results;
        });
    //return results;
}
exports.fontsOfInterest = async (url, lat, long, r) => {
    let results = [];
    return fetch(urlDegrees)
        .then(resp => resp.json())
        .then((data) => {
            data.features.map(feature => {
                let coordinates = [];
                if (feature.geometry != null)
                    coordinates = feature.geometry.coordinates;
                //if (Math.abs(coordinates[0] - lat) < r && Math.abs(coordinates[1] - long) < r) {
                if (r >= distBetween(lat, long, coordinates[1], coordinates[0])) {
                    const font = {
                        id: feature.id,
                        coordinates: coordinates,
                        nombre: feature.properties.nombre || "no info",
                    };
                    results.push(font);
                }
            });
            console.log(results);
            return results;
        });
};

// Result in meters
function distBetween(lat1, lng1, lat2, lng2) {
    //console.log()
    const R = 6371;
    const dLat = deg2rad(lat2 - lat1);
    const dLon = deg2rad(lng2 - lng1);
    const a =
        Math.sin(dLat / 2) * Math.sin(dLat / 2) +
        Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
        Math.sin(dLon / 2) * Math.sin(dLon / 2)
    ;
    const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
    const d = R * c;

    return d*1000;
}

function deg2rad(deg) {
    return deg * (Math.PI / 180)
}