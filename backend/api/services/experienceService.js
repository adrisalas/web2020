const commentService = require("./commentService");
const graffitiService = require("./graffitiService");
const routeService = require("./routeService");

exports.getExperienceByUserId = async (id) => {
  let experience = 0;
  const comments = await commentService.findByUser(id);
  for (c of comments) {
    experience += c.votes.positives.length;
    experience -= c.votes.negatives.length;
  }

  const graffitis = await graffitiService.findQuery({
    "author.author_id": id,
  });
  for (g of graffitis) {
    experience += g.votes.positives.length;
    experience -= g.votes.negatives.length;
  }

  const routes = await routeService.findByUser(id);
  for (r of routes) {
    experience += r.votes.positives.length;
    experience -= r.votes.negatives.length;
  }
  return experience;
};
