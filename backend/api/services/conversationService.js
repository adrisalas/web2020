const mongoose = require('mongoose')
const Conversation = require('../models/conversation')
const AllFacade = require("../dao/AllFacade");
const ConversationFacade = require('../dao/ConversationFacade')


exports.findAll = async ()=>{
    return await ConversationFacade.findAll(Conversation);
};

exports.findById = async (id)=>{
    return await AllFacade.findById(Conversation, id);
};

exports.create = async (conv) => {
    if(conv.users.length === 2){
        return await ConversationFacade.create(Conversation, conv);
    }else{
        const error = new Error("Invalid format for conversation");
        error.status = 400;
        throw error;
    }
};

exports.delete = async (id) => {
    return await AllFacade.delete(Conversation, id);
};

exports.edit = async (id, conv) => {
    return await AllFacade.put(Conversation, id, conv);
};

exports.send = async (id,msg) => {
    return await ConversationFacade.send(Conversation, id, msg);
};
exports.findByUserId = (userId) => {
    return AllFacade.findQuery(Conversation, {users: {$elemMatch: {_id: userId}}});
};

exports.findByLoggedUser = (username) => {
    return AllFacade.findQuery(Conversation, {users: {$elemMatch: {nickname: username}}});
};

exports.findByTwoUsers = async (u1, u2) => {
    console.log(u1);
    console.log(u2);
    const convs = await AllFacade.findQuery(Conversation, {users: {$elemMatch: {_id: u1}}});
    console.log(convs);
    const rdo = [];
    for (const c of convs) {
        console.log(c.users[0]._id);
        console.log(c.users[1]._id);
        if((c.users[0]._id == u1 && c.users[1]._id == u2) ||
            (c.users[1]._id == u1 && c.users[0]._id == u2)){
            rdo.push(c);
        }
    }
    console.log("RDO");
    console.log(rdo);
    return rdo;
};