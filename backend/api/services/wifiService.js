const wifiFacade = require("../dao/wifiFacade");
const fontsFacade = require("../dao/fuentesFacade");
const parksFacade = require("../dao/parksFacade");

exports.findAll = async (url) => {
    return await wifiFacade.findAll(url);
}

exports.findAllFonts = async (url) => {
    return await fontsFacade.findAll(url);
}

exports.findAllParks= async (url) => {
    return await parksFacade.findAll(url);
}

exports.wifiPointsOfInterest = async (url, lat, long, r) => {
    return await wifiFacade.wifiPointsOfInterest(url, lat, long, r);
};

exports.fontsOfInterest = async (url, lat, long, r) => {
    return await fontsFacade.fontsOfInterest(url, lat, long, r);
};

exports.parksOfInterest = async (url, lat, long, r) => {
    return await parksFacade.parksOfInterest(url, lat, long, r);
};