const jwt = require("jsonwebtoken");

exports.checkTokenMW = (req, res, next) => {
  const bearerHeader = req.headers["authorization"];
  if (typeof bearerHeader !== "undefined") {
    jwt.verify(
      bearerHeader.split(" ")[1],
      process.env.JWT_SECRET,
      (err, authData) => {
        if (err || !("_id" in authData)) {
          res.sendStatus(403);
        } else {
          req.authData = authData;
          next();
        }
      }
    );
  } else {
    res.sendStatus(403);
  }
};
