const weatherFacade = require("../dao/weatherFacade");

exports.findByCoordinates = async (lat, lon) => {
  return await weatherFacade.findByCoordinates(lat, lon);
};
