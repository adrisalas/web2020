const imgurFacade = require("../dao/imgurFacade");

exports.upload = async (image) => {
  return await imgurFacade.upload(image);
};
