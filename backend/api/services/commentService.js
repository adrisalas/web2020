const mongoose = require("mongoose");
const Comment = require("../models/comment");
const AllFacade = require("../dao/AllFacade");
const GraffitiFacade = require("../dao/graffitiFacade");

exports.findAll = async () => {
  return await AllFacade.find(Comment);
};

exports.findById = async (id) => {
  return await AllFacade.findById(Comment, id);
};

exports.findByGraffiti = async (graffitiId) => {
  return await AllFacade.findQuery(Comment, {
    "parent.type": "Graffiti",
    "parent._id": graffitiId,
  });
};

exports.findByRoute = async (routeId) => {
  return await AllFacade.findQuery(Comment, {
    "parent.type": "Route",
    "parent._id": routeId,
  });
};

exports.findByUser = async (userId) => {
  return await AllFacade.findQuery(Comment, {
    "author._id": userId,
  });
};

exports.create = async (comment) => {
  if (!comment.message || !comment.parent || !comment.author) {
    const error = new Error("Invalid format for comment");
    error.status = 400;
    throw error;
  }
  return await AllFacade.create(
    Comment,
    new Comment({
      _id: new mongoose.Types.ObjectId(),
      message: comment.message,
      author: comment.author,
      parent: comment.parent,
      date: new Date(),
      votes: { positives: [], negatives: [] },
    })
  );
};

exports.delete = async (id) => {
  return await AllFacade.delete(Comment, id);
};

exports.edit = async (id, comment) => {
  return await AllFacade.put(Comment, id, comment);
};

exports.addPositiveVote = async (id, userId) => {
  return await GraffitiFacade.addPositiveVote(Comment, id, userId);
};

exports.addNegativeVote = async (id, userId) => {
  return await GraffitiFacade.addNegativeVote(Comment, id, userId);
};

exports.removePositiveVote = async (id, userId) => {
  return await GraffitiFacade.removePositiveVote(Comment, id, userId);
};

exports.removeNegativeVote = async (id, userId) => {
  return await GraffitiFacade.removeNegativeVote(Comment, id, userId);
};
