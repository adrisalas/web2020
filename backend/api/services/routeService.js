const Route = require("../models/route");
const AllFacade = require("../dao/AllFacade");
const routeFacade = require("../dao/routeFacade");
const voteFacade = require("../dao/voteFacade");
const userService = require("./userService");
const fetch = require("node-fetch");

const graffitiService = require("./graffitiService");
let location = {};
exports.findAll = async () => {
  return await AllFacade.find(Route);
};

exports.findById = async (id) => {
  return await AllFacade.findById(Route, id);
};

exports.findByUser = async (userId) => {
  return await AllFacade.findQuery(Route, {
    "author.author_id": userId,
  });
};

exports.create = async (object) => {
  object.date = Date.now();
  object.author.name = (
    await userService.findById(object.author.author_id)
  ).name;

  const location = await getLatLon(object);
  object.location = location;
  return AllFacade.create(Route, object);
};

exports.delete = async (id) => {
  return await AllFacade.delete(Route, id);
};

exports.deleteAll = async () => {
  return await AllFacade.deleteAll(Route);
};

exports.put = async (id, object) => {
  const location = await getLatLon(object);
  object.graffitis = [];
  object.location = location;
  return AllFacade.put(Route, id, object);
};

exports.addPositiveVote = async (id, userId) => {
  return await voteFacade.addPositiveVote(Route, id, userId);
};

exports.addNegativeVote = async (id, userId) => {
  return await voteFacade.addNegativeVote(Route, id, userId);
};

exports.removePositiveVote = async (id, userId) => {
  return await voteFacade.removePositiveVote(Route, id, userId);
};

exports.removeNegativeVote = async (id, userId) => {
  return await voteFacade.removeNegativeVote(Route, id, userId);
};

exports.addGraffiti = async (id, graffitiId) => {
  const object = await graffitiService.findById(graffitiId);
  return await routeFacade.addGraffiti(Route, id, object);
};

exports.deleteAllGraffitis = async (id) => {
  return await routeFacade.deleteAllGraffitis(Route, id);
};

const getLatLon = async (object) => {
  try {
    location = {};
    let ciudad = object.city;
    let requestOptions = {
      method: "GET",
      headers: { Accept: "application/json" },
      redirect: "follow",
    };
    let url =
      "https://nominatim.openstreetmap.org/search?q=" +
      ciudad +
      "&format=json&addressdetails=1";
    url = encodeURI(url); // para poder usar tildes
    return fetch(url, requestOptions)
      .then((response) => response.json())
      .then((data) => {
        const datos = data[0];

        if (datos) {
          location = {
            lat: datos.lat,
            lon: datos.lon,
          };
          object.location = location;
        }

        return location;
      })
      .catch((error) => console.log(error));
  } catch (err) {
    console.log(err);
  }
};
