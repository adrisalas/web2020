import "./App.css";
import React from "react";
import RouterSetup from "./components/nav-bar/RouterSetup";

function App() {
  return <RouterSetup />;
}

export default App;
