import React, { useState, useEffect } from "react";
import Container from "react-bootstrap/Container";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";
import TextArea from "./TextArea";
import Comment from "./Comment.js";

const Comments = (props) => {
  const [comments, setComments] = useState([]);

  let userName = props.userName;
  let userId = props.userId || "";
  let type = props.type; //It can be 'Graffiti' or 'Route'
  let typeId = props.typeId;

  let updateComments = () => {
    let url =
      type === "Graffiti"
        ? process.env.REACT_APP_BACKEND_URL + "/comments?graffiti="
        : process.env.REACT_APP_BACKEND_URL + "/comments?route=";
    url += typeId;
    fetch(url)
      .then((raw) => raw.json())
      .then((data) => {
        setComments(data);
      })
      .catch((error) => console.log("error", error));
  };

  let deleteComment = (id) => {
    var requestOptions = {
      method: "DELETE",
      headers: {
        Authorization: "Bearer " + sessionStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      redirect: "follow",
    };

    fetch(process.env.REACT_APP_BACKEND_URL + "/comments/" + id, requestOptions)
      .then(() => updateComments())
      .catch((error) => console.log("error", error));
  };

  useEffect(() => {
    updateComments();
  }, []); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <Container>
      {userId !== "" ? (
        <Row>
          <TextArea
            authorName={userName}
            authorId={userId}
            type={type}
            typeId={typeId}
            commentSent={updateComments}
          ></TextArea>
        </Row>
      ) : undefined}
      <Row>
        <Col>
          {comments.map((c, index) => (
            <Comment
              key={c._id}
              comment={c}
              deleteComment={deleteComment}
              userId={userId}
            ></Comment>
          ))}
        </Col>
      </Row>
    </Container>
  );
};

export default Comments;
