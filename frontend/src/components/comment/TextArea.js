import React, { useState } from "react";
import Col from "react-bootstrap/Col";
import Form from "react-bootstrap/Form";
import Button from "react-bootstrap/Button";

export default (props) => {
  const [textArea, setTextArea] = useState("");

  let authorId = props.authorId;
  let authorName = props.authorName;
  let type = props.type;
  let typeId = props.typeId;

  let handleSubmit = (e) => {
    e.preventDefault();

    var raw = JSON.stringify({
      message: textArea,
      author: {
        _id: authorId,
        name: authorName,
        image: JSON.parse(sessionStorage.getItem("user")).image,
      },
      parent: { type: type, _id: typeId },
    });

    var requestOptions = {
      method: "POST",
      headers: {
        Authorization: "Bearer " + sessionStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      body: raw,
      redirect: "follow",
    };

    fetch(process.env.REACT_APP_BACKEND_URL + "/comments/", requestOptions)
      .then((response) => response.text())
      .then(() => {
        props.commentSent();
        setTextArea("");
        console.log("TODO: Show success toast");
      })
      .catch((error) => console.log(error, "error"));
  };

  return (
    <Col>
      <Form>
        <Form.Group>
          <Form.Label>
            Comment as <a href={"/profile/"}>{authorName}</a>
          </Form.Label>
          <Form.Control
            as="textarea"
            rows={2}
            onChange={(event) => setTextArea(event.target.value)}
            value={textArea}
          />
        </Form.Group>
        <Button variant="primary" type="submit" onClick={handleSubmit}>
          Submit
        </Button>
      </Form>
    </Col>
  );
};
