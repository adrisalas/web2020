import React, { useState, useEffect } from "react";
import Row from "react-bootstrap/Row";

const Votes = (props) => {
  const [votes, setVotes] = useState(props.votes);
  let userId = props.userId || "";
  let type = props.type;

  const [upvoteSelected, setUpvoteSelected] = useState(false);
  const [downvoteSelected, setDownvoteSelected] = useState(false);

  let sendVote = (method, isUpvote) => {
    let url =
      process.env.REACT_APP_BACKEND_URL +
      "/" +
      type +
      "/" +
      props.id +
      "/votes/";
    url += isUpvote === true ? "positives" : "negatives";

    var raw = JSON.stringify({ _id: userId });
    var requestOptions = {
      method: method,
      headers: {
        Authorization: "Bearer " + sessionStorage.getItem("token"),
        "Content-Type": "application/json",
      },
      body: raw,
      redirect: "follow",
    };
    if (userId !== "") {
      fetch(url, requestOptions)
        .then(() => {
          let newVotes = { positives: [], negatives: [] };
          if (isUpvote) {
            let found = false;
            for (let u of votes.negatives) {
              if (u._id !== userId) {
                newVotes.negatives.push(u);
              }
            }
            for (let u of votes.positives) {
              if (u._id === userId) {
                found = true;
              } else {
                newVotes.positives.push(u);
              }
            }
            if (!found) {
              newVotes.positives.push({ _id: userId });
            }
          } else {
            let found = false;
            for (let u of votes.positives) {
              if (u._id !== userId) {
                newVotes.positives.push(u);
              }
            }
            for (let u of votes.negatives) {
              if (u._id === userId) {
                found = true;
              } else {
                newVotes.negatives.push(u);
              }
            }
            if (!found) {
              newVotes.negatives.push({ _id: userId });
            }
          }
          setVotes(newVotes);
          props.update(newVotes);
        })
        .catch((error) => console.log("error", error));
    } else {
      alert("Please log in to vote");
    }
  };

  let upvoteNoFill = (
    <i style={{ cursor: "pointer" }} onClick={() => sendVote("POST", true)}>
      <svg
        width="1.5em"
        height="1.5em"
        viewBox="0 0 16 16"
        className="bi bi-arrow-up-circle"
        fill="currentColor"
        xmlns="http://www.w3.org/2000/svg"
        color="green"
      >
        <path
          fillRule="evenodd"
          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
        />
        <path
          fillRule="evenodd"
          d="M8 12a.5.5 0 0 0 .5-.5V5.707l2.146 2.147a.5.5 0 0 0 .708-.708l-3-3a.5.5 0 0 0-.708 0l-3 3a.5.5 0 1 0 .708.708L7.5 5.707V11.5a.5.5 0 0 0 .5.5z"
        />
      </svg>
    </i>
  );
  let upvoteFill = (
    <i style={{ cursor: "pointer" }} onClick={() => sendVote("DELETE", true)}>
      <svg
        width="1.5em"
        height="1.5em"
        viewBox="0 0 16 16"
        className="bi bi-arrow-up-circle-fill"
        fill="currentColor"
        xmlns="http://www.w3.org/2000/svg"
        color="green"
      >
        <path
          fillRule="evenodd"
          d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zm-7.5 3.5a.5.5 0 0 1-1 0V5.707L5.354 7.854a.5.5 0 1 1-.708-.708l3-3a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8.5 5.707V11.5z"
        />
      </svg>
    </i>
  );

  let downvoteNoFill = (
    <i style={{ cursor: "pointer" }} onClick={() => sendVote("POST", false)}>
      <svg
        width="1.5em"
        height="1.5em"
        viewBox="0 0 16 16"
        className="bi bi-arrow-down-circle"
        fill="currentColor"
        xmlns="http://www.w3.org/2000/svg"
        color="red"
      >
        <path
          fillRule="evenodd"
          d="M8 15A7 7 0 1 0 8 1a7 7 0 0 0 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"
        />
        <path
          fillRule="evenodd"
          d="M8 4a.5.5 0 0 1 .5.5v5.793l2.146-2.147a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 1 1 .708-.708L7.5 10.293V4.5A.5.5 0 0 1 8 4z"
        />
      </svg>
    </i>
  );

  let downvoteFill = (
    <i style={{ cursor: "pointer" }} onClick={() => sendVote("DELETE", false)}>
      <svg
        width="1.5em"
        height="1.5em"
        viewBox="0 0 16 16"
        className="bi bi-arrow-down-circle-fill"
        fill="currentColor"
        xmlns="http://www.w3.org/2000/svg"
        color="red"
      >
        <path
          fillRule="evenodd"
          d="M16 8A8 8 0 1 1 0 8a8 8 0 0 1 16 0zM8.5 4.5a.5.5 0 0 0-1 0v5.793L5.354 8.146a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0l3-3a.5.5 0 0 0-.708-.708L8.5 10.293V4.5z"
        />
      </svg>
    </i>
  );
  useEffect(() => {
    setUpvoteSelected(
      votes.positives.reduce((exist, v) => {
        return v._id === userId ? true : exist;
      }, false)
    );
    setDownvoteSelected(
      votes.negatives.reduce((exist, v) => {
        return v._id === userId ? true : exist;
      }, false)
    );
  }, [votes]); // eslint-disable-line react-hooks/exhaustive-deps

  return (
    <>
      <Row className="justify-content-center mt-1" key={votes}>
        {upvoteSelected ? upvoteFill : upvoteNoFill}
        {props.displayVotes ? (
          votes.positives ? (
            <p
              style={{
                marginRight: ".5rem",
                marginLeft: ".5rem",
                marginTop: ".1rem",
              }}
            >
              {votes.positives.length - votes.negatives.length}
            </p>
          ) : (
            <p>0</p>
          )
        ) : (
          ""
        )}
        {downvoteSelected ? downvoteFill : downvoteNoFill}
      </Row>
    </>
  );
};

export default Votes;
