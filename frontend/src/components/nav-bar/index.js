import React, { useState } from "react";
import { Navbar, Nav, NavDropdown } from "react-bootstrap";
import { Link, useHistory } from "react-router-dom";
import Weather from "../commons/weather";
import { useLocation } from "../../custom-hooks/useLocation";
import "./navbar.css";
import { GoogleLogin } from "react-google-login";
import ChuckNorris from "../chucknorris";

const authRoute = process.env.REACT_APP_BACKEND_URL + "/googlelogin";

const NavBarMain = () => {
  const location = useLocation();
  const [registrado, setRegistrado] = useState(sessionStorage.getItem("token"));
  const [userName, setUserName] = useState(
    sessionStorage.getItem("user")
      ? JSON.parse(sessionStorage.getItem("user")).name
      : ""
  );
  const [style, setStyle] = useState(true);

  const history = useHistory();

  const signOut = () => {
    sessionStorage.clear();
    setRegistrado(false);
    setUserName(undefined);
    history.push("/");
  };

  const responseGoogle = async (response) => {
    if (response.profileObj) {
      const idToken = { idtoken: response.tokenId };
      await fetch(authRoute, {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(idToken),
        redirect: "follow",
      })
        .then((res) => {
          return res.json();
        })
        .then((data) => {
          setRegistrado(true);
          setUserName(data.user.name);
          sessionStorage.setItem("user", JSON.stringify(data.user));
          sessionStorage.setItem("token", data.token);
          window.location.reload(false);
        });
    }
  };

  const updateStyle = () => {
    setStyle(!style);
  };

  return (
    <>
      <Navbar
        collapseOnSelect
        expand="lg"
        bg="dark"
        variant="dark"
        style={style ? { height: "4rem" } : {}}
      >
        <Navbar.Brand as={Link} to="/">
          Graffiti-App
        </Navbar.Brand>
        <Navbar.Toggle
          aria-controls="responsive-navbar-nav"
          onClick={() => updateStyle()}
        />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="mr-auto">
            {registrado ? (
              <Nav.Link as={Link} to="/routes">
                Routes
              </Nav.Link>
            ) : (
              <></>
            )}
            <Nav.Link as={Link} to="/opendata">
              Open Data
            </Nav.Link>
            <Nav.Link as={Link} to="/graffitismap">
              GraffitisMap
            </Nav.Link>
          </Nav>

          <Nav className="auto" style={{ paddingTop: "1rem" }}>
            <Weather
              lat={location.lat || 36.721261}
              lon={location.lon || -4.4212655}
            />
          </Nav>

          <Nav className="ml-auto">
            {registrado ? (
              <>
                <Nav.Link as={Link} to="/conversations">
                  Conversations
                </Nav.Link>
                <Nav.Link as={Link} to="/graffitis">
                  Upload
                </Nav.Link>
              </>
            ) : (
              <></>
            )}
            {!registrado ? (
              <GoogleLogin
                clientId={process.env.REACT_APP_GOOGLE_CLIENT_ID}
                buttonText="Login"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={"single_host_origin"}
              />
            ) : (
              <NavDropdown title={userName} id="basic-nav-dropdown">
                <NavDropdown.Item href="/profile">Profile</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item
                  onClick={() => {
                    signOut();
                  }}
                >
                  Log out
                </NavDropdown.Item>
              </NavDropdown>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
      <ChuckNorris></ChuckNorris>
    </>
  );
};
export default NavBarMain;
