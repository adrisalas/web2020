import React, { useState, useEffect } from "react";
import Graffitis from "./Graffitis";
import { useFetch } from "../../custom-hooks/useFetch";
import {
  Container,
  Accordion,
  Form,
  Button,
  Card,
  Row,
  Col,
  Pagination,
} from "react-bootstrap";

const url = process.env.REACT_APP_BACKEND_URL + "/graffitis";

// Graffitis per page
const gpp = 12;

const GraffitiList = () => {
  const { products } = useFetch(url);
  const [graffitis, setGraffitis] = useState([]);
  const [filter, setFilter] = useState({
    user: "",
    artist: "",
    date: new Date(),
  });
  const [onPage, setOnPage] = useState(1);
  const [items, setItems] = useState([]);
  const [pages, setPages] = useState(1);
  const [order, setOrder] = useState(0);

  const changePage = (n) => {
    setOnPage(n);
    paginate();
  };

  const paginate = () => {
    let newItems = [];
    for (let number = 1; number <= pages; number++) {
      newItems.push(
        <Pagination.Item
          key={number}
          active={number === onPage}
          onClick={number !== onPage ? () => changePage(number) : null}
        >
          {number}
        </Pagination.Item>
      );
    }
    setItems(newItems);
  };

  useEffect(() => {
    setGraffitis(products);
    if (products.length > gpp) {
      setPages(Math.ceil(products.length / gpp));
      paginate();
    }
  }, [products, onPage, pages]);

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setFilter({ ...filter, [name]: value });
  };

  const compareDates = (date) => {
    const d1 = new Date(date);
    const d2 = new Date(filter.date);
    return (
      d1.getDay() === d2.getDay() &&
      d1.getMonth() === d2.getMonth() &&
      d1.getFullYear() === d2.getFullYear()
    );
  };

  const clearFilter = () => {
    setFilter({
      user: "",
      artist: "",
      date: new Date(),
    });
  };

  const orderBy = (str) => {
    if (str === "date") {
      if (order === 1) {
        const ordered = graffitis.sort((a, b) =>
          new Date(a.date).getTime() < new Date(b.date).getTime() ? 1 : -1
        );
        setOrder(0);
        setGraffitis(ordered);
      } else {
        const ordered = graffitis.sort((a, b) =>
          new Date(a.date).getTime() > new Date(b.date).getTime() ? 1 : -1
        );
        setOrder(1);
        setGraffitis(ordered);
      }
    } else if (str === "votes") {
      if (order === -1) {
        const ordered = graffitis.sort((a, b) => {
          const aVotes = a.votes.positives
            ? a.votes.positives.length - a.votes.negatives.length
            : null;
          const bVotes = b.votes.positives
            ? b.votes.positives.length - b.votes.negatives.length
            : null;
          if (aVotes > bVotes) {
            return 1;
          } else if (aVotes < bVotes) {
            return -1;
          } else {
            return 0;
          }
        });
        setGraffitis(ordered);
        setOrder(0);
      } else {
        const ordered = graffitis.sort((a, b) => {
          const aVotes = a.votes.positives
            ? a.votes.positives.length - a.votes.negatives.length
            : null;
          const bVotes = b.votes.positives
            ? b.votes.positives.length - b.votes.negatives.length
            : null;
          if (aVotes > bVotes) {
            return -1;
          } else if (aVotes < bVotes) {
            return 1;
          } else {
            return 0;
          }
        });
        setGraffitis(ordered);
        setOrder(-1);
      }
    }
  };

  if (graffitis) {
    return (
      <Container>
        <h1>Graffitis</h1>

        <Row>
          <Col>
            <Accordion style={{ marginTop: "2rem" }}>
              <Card>
                <Card.Header>
                  <Accordion.Toggle as={Button} variant="link" eventKey="0">
                    Filtros
                  </Accordion.Toggle>
                </Card.Header>
                <Accordion.Collapse eventKey="0">
                  <Card.Body>
                    <Form>
                      <Form.Row>
                        <Col>
                          <Form.Group controlId="user">
                            <Form.Label>User</Form.Label>
                            <Form.Control
                              name="user"
                              type="text"
                              value={filter.user}
                              onChange={handleChange}
                            />
                          </Form.Group>
                        </Col>
                        <Col>
                          <Form.Group controlId="artist">
                            <Form.Label>Artist</Form.Label>
                            <Form.Control
                              type="text"
                              name="artist"
                              value={filter.artist}
                              onChange={handleChange}
                            />
                          </Form.Group>
                        </Col>
                        <Col>
                          <Form.Group controlId="date">
                            <Form.Label>Date</Form.Label>
                            <Form.Control
                              name="date"
                              type="date"
                              value={filter.date}
                              onChange={handleChange}
                            />
                          </Form.Group>
                        </Col>
                      </Form.Row>
                      <Button variant="secondary" onClick={() => clearFilter()}>
                        Clear
                      </Button>
                    </Form>
                  </Card.Body>
                </Accordion.Collapse>
              </Card>
            </Accordion>
          </Col>
        </Row>
        <Row style={{ marginTop: "1.5rem" }}>
          <Col>
            Order by:{"  "}
            <Button
              onClick={() => orderBy("date")}
              variant={order === 1 ? "primary" : "outline-primary"}
            >
              Date
            </Button>
            {"  "}
            <Button
              onClick={() => orderBy("votes")}
              variant={order === -1 ? "primary" : "outline-primary"}
            >
              Votes
            </Button>
          </Col>
        </Row>

        <section className="products">
          {graffitis.slice(gpp * onPage - gpp, gpp * onPage).map((graffiti) => {
            if (
              graffiti.author.name
                .toLowerCase()
                .includes(filter.user.toLowerCase()) &&
              graffiti.artist
                .toLowerCase()
                .includes(filter.artist.toLowerCase()) &&
              (filter.date <= new Date() || compareDates(graffiti.date))
            ) {
              return <Graffitis key={graffiti._id} {...graffiti} />;
            } else {
              return <></>;
            }
          })}
        </section>

        <Pagination>{items}</Pagination>
      </Container>
    );
  } else {
    return <div>There're no graffitis yet</div>;
  }
};

export default GraffitiList;
