import React, { useState, useReducer } from "react";
import { Container, Form, Col, Button } from "react-bootstrap";
import { reducer } from "./reducer";
import { MapContainer, Marker, TileLayer, useMapEvents } from "react-leaflet";
import Modal from "./Modal";
import uploadImage from "../commons/uploadImage";

const initialState = {
  isModalOpen: false,
  modalContent: "",
};

const url = process.env.REACT_APP_BACKEND_URL + "/graffitis/";

const searchLocation = (location) => {
  return fetch(
    process.env.REACT_APP_BACKEND_URL +
      `/nominating/reverse?lon=${location.lon}&lat=${location.lat}`
  )
    .then((res) => res.json())
    .then((reverse) => reverse.display_name);
};

const createGraffiti = async (graffiti) => {
  const locationName = await searchLocation(graffiti.location);
  const newGraffiti = {
    ...graffiti,
    location: { ...graffiti.location, name: locationName },
  };
  await fetch(url, {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(newGraffiti),
    redirect: "follow",
  });
};

const updateGraffiti = async (graffiti, update) => {
  const locationName = await searchLocation(graffiti.location);
  const newGraffiti = {
    ...graffiti,
    location: { ...graffiti.location, name: locationName },
  };
  await fetch(url + graffiti._id, {
    method: "PUT",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify(newGraffiti),
    redirect: "follow",
  });
  update(graffiti);
};

const GraffitiForm = (props) => {
  const [state, dispatch] = useReducer(reducer, initialState);
  const [validated, setValidated] = useState(false);
  const [newTag, setNewTag] = useState("");
  const [position, setPosition] = useState();

  const initialize = () => {
    //{ author_id: "5fc239bd81249d00176aa6d0", name: "ElPepe" },

    try {
      const { _id, name } = JSON.parse(sessionStorage.getItem("user"));
      return {
        title: "",
        author: { author_id: _id, name },
        description: "",
        artist: "",
        image: "",
        state: "New",
        tags: [],
        location: { lat: 0, lon: 0, name: "" },
      };
    } catch (err) {
      window.location.replace("/");
      return null;
    }
  };

  const [graffiti, setGraffiti] = useState(props.editGraffiti || initialize());

  const greeting = props.editGraffiti ? (
    <h2 style={{ marginBottom: "2rem" }}>Edit graffiti</h2>
  ) : (
    <h2 style={{ marginBottom: "2rem" }}>New Graffiti</h2>
  );
  const submit = props.editGraffiti ? "Edit" : "Submit";

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    if (name === "lat" || name === "lon") {
      setGraffiti({
        ...graffiti,
        location: { ...graffiti.location, [name]: value },
      });
    } else if (name === "tag") {
      setNewTag(value);
    } else {
      setGraffiti({ ...graffiti, [name]: value });
    }
  };

  const handleSubmit = (e) => {
    const form = e.currentTarget;
    e.preventDefault();

    if (form.checkValidity() === false) {
      e.stopPropagation();
    }

    if (
      graffiti.title &&
      graffiti.author &&
      graffiti.description &&
      graffiti.image
    ) {
      if (graffiti._id) {
        updateGraffiti(graffiti, props.update);
        dispatch({ type: "UPDATE_ITEM" });
        setValidated(true);
      } else {
        createGraffiti(graffiti);
        dispatch({ type: "ADD_ITEM" });
        setGraffiti(initialize());
        setValidated(false);
      }
    } else {
      dispatch({ type: "NO_VALUE" });
      setValidated(true);
    }
  };

  const closeModal = () => {
    dispatch({ type: "CLOSE_MODAL" });
  };

  const addTag = () => {
    setGraffiti({ ...graffiti, tags: [...graffiti.tags, newTag] });
    setNewTag("");
  };

  function AddMarkerToClick() {
    useMapEvents({
      click(e) {
        const newPosition = e.latlng;
        setPosition(newPosition);
        setGraffiti({
          ...graffiti,
          location: {
            ...graffiti.location,
            lat: newPosition.lat,
            lon: newPosition.lng,
          },
        });
      },
    });

    if (position) {
      return <Marker position={position} />;
    } else {
      return <></>;
    }
  }

  return (
    <Container>
      {greeting}
      {!state.isModalOpen || (
        <Modal closeModal={closeModal} modalContent={state.modalContent} />
      )}
      <Form
        noValidate
        validated={validated}
        onKeyPress={(e) => (e.key === "Enter" ? e.preventDefault() : null)}
      >
        <Form.Group controlId="title">
          <Form.Label>Title</Form.Label>
          <Form.Control
            required
            type="text"
            name="title"
            value={graffiti.title}
            onChange={handleChange}
          />
          <Form.Control.Feedback type="invalid">
            Please insert a title.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group controlId="description">
          <Form.Label>Description</Form.Label>
          <Form.Control
            required
            as="textarea"
            rows={3}
            name="description"
            value={graffiti.description}
            onChange={handleChange}
          />
          <Form.Control.Feedback type="invalid">
            Please insert a description.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group controlId="image">
          <Form.Label>Image</Form.Label>
          <br />
          {graffiti.image !== "" ? (
            <img
              style={{ maxWidth: "20rem", maxHeight: "20rem" }}
              id="imagen-imgur"
              src={graffiti.image}
              name="imagen-imgur"
              alt="uploaded"
            />
          ) : (
            <></>
          )}
          <Form.Control
            required
            type="file"
            name="image"
            className="input-image"
            onChange={() => uploadImage({ graffiti, setGraffiti })}
          />
          <Form.Control.Feedback type="invalid">
            Please insert a image.
          </Form.Control.Feedback>
        </Form.Group>
        <Form.Group controlId="artist">
          <Form.Label>Artist</Form.Label>
          <Form.Control
            type="text"
            name="artist"
            value={graffiti.artist}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group controlId="state">
          <Form.Label>State</Form.Label>
          <Form.Control
            as="select"
            name="state"
            value={graffiti.state}
            onChange={handleChange}
          >
            <option>New</option>
            <option>Old</option>
            <option>Washed</option>
          </Form.Control>
        </Form.Group>
        <Form.Group controlId="tag">
          <Form.Label>
            Tags{" "}
            {graffiti.tags[0] ? (
              <div style={{ fontStyle: "italic" }}>
                Added tags:{" "}
                {graffiti.tags.map((tag) => {
                  if (tag === graffiti.tags[graffiti.tags.length - 1]) {
                    return tag;
                  } else {
                    return tag + ", ";
                  }
                })}
              </div>
            ) : (
              ""
            )}
          </Form.Label>
          <Form.Control
            type="text"
            name="tag"
            value={newTag}
            onChange={handleChange}
          />
          <Button
            variant="secondary"
            style={{ marginTop: ".5rem" }}
            onClick={addTag}
          >
            Add
          </Button>
        </Form.Group>
        <h4 style={{ marginTop: "2rem" }}>Location</h4>
        <Form.Row>
          <MapContainer
            id="graffitimap"
            key={[36.721261, -4.4212655]}
            center={[36.721261, -4.4212655]}
            zoom={13}
            scrollWheelZoom={true}
            onClick={(e) => console.log(e)}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            <AddMarkerToClick />
          </MapContainer>
        </Form.Row>
        <Form.Row style={{ marginTop: "1rem" }}>
          <Form.Group as={Col} controlId="lat">
            <Form.Label>Latitude</Form.Label>
            <Form.Control
              type="number"
              name="lat"
              value={graffiti.location.lat}
              onChange={handleChange}
            />
          </Form.Group>

          <Form.Group as={Col} controlId="lon">
            <Form.Label>Longitude</Form.Label>
            <Form.Control
              type="number"
              name="lon"
              value={graffiti.location.lon}
              onChange={handleChange}
            />
          </Form.Group>
        </Form.Row>

        <Button
          type="submit"
          style={{ marginBottom: "2rem" }}
          onClick={handleSubmit}
        >
          {submit}
        </Button>
      </Form>
    </Container>
  );
};

export default GraffitiForm;
