import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { useFetch } from "../../custom-hooks/useFetch";
import { Image, Container, Row, Col, Button, Tab, Tabs } from "react-bootstrap";
import defaultImg from "../../assets/noImage.jpg";
import GraffitiForm from "./Form";
import Comments from "../comment/Comments";
import Votes from "../comment/Votes";
import Share from "./Share";

const url = process.env.REACT_APP_BACKEND_URL + "/graffitis/";

const Graffiti = () => {
  const [graffiti, setGraffiti] = useState({});
  const [user, setUser] = useState({});
  const [votes, setVotes] = useState({ positives: [], negatives: [] });
  const { _id } = useParams();
  const { products } = useFetch(url + _id);
  const img = graffiti.image || defaultImg;

  useEffect(() => {
    try {
      setUser(JSON.parse(sessionStorage.getItem("user")));
    } catch (err) {}
    setGraffiti(products);
  }, [products]);

  const updateGraffiti = async (newG) => {
    if (newG) {
      setGraffiti(newG);
    } else {
      const response = await fetch(url + _id);
      const newGraffiti = await response.json();
      setGraffiti(newGraffiti);
    }
  };

  const removeGraffiti = (id) => {
    fetch(url + id, {
      method: "DELETE",
    });
  };

  const details = graffiti.author ? (
    <>
      <Row style={{ marginTop: "2rem" }}>
        <Col>
          <b>Description:</b> {graffiti.description}
        </Col>
      </Row>
      <Row style={{ marginTop: "1rem" }}>
        <Col>
          <b>Artist:</b> {graffiti.artist ? graffiti.artist : "Anonymous"}
        </Col>
        <Col>
          <b>Location: </b>
          {graffiti.location.name ? graffiti.location.name : "Unknown"}
        </Col>
      </Row>
      <Row>
        <Col>
          <b>Uploaded: </b>
          {new Date(graffiti.date).toLocaleDateString("es-ES")}
        </Col>
        <Col>
          <b>State:</b> {graffiti.state ? graffiti.state : "New"}
        </Col>
      </Row>
      <Row>
        <Col>
          <b>Tags: </b>
          {graffiti.tags[0]
            ? graffiti.tags.map((tag) => {
                if (tag === graffiti.tags[graffiti.tags.length - 1]) {
                  return tag;
                } else {
                  return tag + ", ";
                }
              })
            : "None"}
        </Col>
      </Row>
      <Row style={{ marginTop: "1rem" }}>
        <Col>
          <b>Share:</b>
          <Row style={{ marginLeft: "1rem" }}>
            <Share id={graffiti._id} />
          </Row>
        </Col>
      </Row>
      <Row>
        <Col>
          <hr
            style={{
              marginLeft: "10%",
              marginRight: "10%",
              marginTop: "1%",
            }}
          />
          <h3>Comments</h3>
          <Comments
            userName={user?.name}
            userId={user?._id}
            type="Graffiti"
            typeId={graffiti._id}
          />
          <br />
        </Col>
      </Row>
    </>
  ) : (
    <></>
  );

  if (graffiti.author) {
    return (
      <Container>
        <Row>
          <Col>
            <h1>{graffiti.title}</h1>
          </Col>
        </Row>
        <Row>
          <Col sm={10}>
            <h2>
              By{" "}
              <a
                href={"/profile/" + graffiti.author.author_id}
                style={{ color: "inherit" }}
              >
                {graffiti.author.name}
              </a>
            </h2>
          </Col>
          {user && graffiti.author && graffiti.author.author_id === user._id ? (
            <Col sm={2}>
              <Button
                variant="danger"
                onClick={() => removeGraffiti(graffiti._id)}
              >
                <Link
                  to="/"
                  style={{ color: "inherit", textDecoration: "inherit" }}
                >
                  Delete
                </Link>
              </Button>
            </Col>
          ) : (
            <></>
          )}
        </Row>
        <Row className="justify-content-md-center">
          <Image
            style={{ marginTop: "2rem", maxHeight: "40rem", maxWidth: "40rem" }}
            src={img}
            alt={graffiti.title}
          />
        </Row>
        <Row>
          <Col>
            {votes ? (
              <Votes
                key={graffiti.votes}
                userId={user?._id}
                votes={graffiti.votes}
                id={graffiti._id}
                type="graffitis"
                displayVotes={true}
              />
            ) : undefined}
          </Col>
        </Row>
        {user && graffiti.author && graffiti.author.author_id === user._id ? (
          <Tabs>
            <Tab eventKey="details" title="Details">
              {details}
            </Tab>
            <Tab
              eventKey="edit"
              mountOnEnter={true}
              title="Edit"
              disabled={
                !(
                  user &&
                  graffiti.author &&
                  graffiti.author.author_id === user._id
                )
              }
            >
              <GraffitiForm editGraffiti={graffiti} update={updateGraffiti} />
            </Tab>
          </Tabs>
        ) : (
          <>
            <hr
              style={{
                marginLeft: "10%",
                marginRight: "10%",
                marginTop: "1%",
              }}
            />
            {details}
          </>
        )}
      </Container>
    );
  } else {
    return <></>;
  }
};

export default Graffiti;
