import React, { useState, useEffect } from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import { useFetch } from "../../custom-hooks/useFetch";
import { useLocation } from "../../custom-hooks/useLocation";

const url = process.env.REACT_APP_BACKEND_URL + "/graffitis";

const Map = () => {
  const location = useLocation();
  const [center, setCenter] = useState([]);
  const [graffitis, setGraffitis] = useState([]);
  const { products } = useFetch(url);

  useEffect(() => {
    setGraffitis(products);
    setCenter([location.lat || 36.721261, location.lon || -4.4212655]);
  }, [products, location]);

  const selectLocation = (gra) => {
    setCenter([gra.location.lat, gra.location.lon]);
  };

  const table = graffitis.map((gra) => {
    return (
      <tr onClick={() => selectLocation(gra)}>
        <td>{gra.title}</td>
        <td>{gra.author.name}</td>
        <td>{new Date(gra.date).toLocaleDateString("es-ES")}</td>
      </tr>
    );
  });

  const map = graffitis.map((gra) => {
    return (
      <Marker key={gra._id} position={[gra.location.lat, gra.location.lon]}>
        <Popup>
          {gra.title}
          <br />
          <img
            style={{ maxWidth: "10rem", maxHeight: "10rem" }}
            src={gra.image}
            alt={gra.title}
          />
        </Popup>
      </Marker>
    );
  });

  return (
    <Container style={{ marginBottom: "5rem" }}>
      <Row>
        <Col>
          <h1>Graffitis</h1>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>{graffitis[0] ? table : "  "}</tbody>
          </Table>
        </Col>
        <Col>
          <h1>Map</h1>
          <MapContainer
            id="graffitimap"
            key={center.length > 0 ? center : [36.721261, -4.4212655]}
            center={center.length > 0 ? center : [36.721261, -4.4212655]}
            zoom={13}
            scrollWheelZoom={true}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {map}
          </MapContainer>
        </Col>
      </Row>
    </Container>
  );
};

export default Map;
