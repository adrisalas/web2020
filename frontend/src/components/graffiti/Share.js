import React, { useState, useEffect } from "react";
import {
  EmailShareButton,
  FacebookShareButton,
  LinkedinShareButton,
  PinterestShareButton,
  RedditShareButton,
  TelegramShareButton,
  TwitterShareButton,
  WhatsappShareButton,
  EmailIcon,
  FacebookIcon,
  LinkedinIcon,
  PinterestIcon,
  RedditIcon,
  TelegramIcon,
  TwitterIcon,
  WhatsappIcon,
} from "react-share";

import Row from "react-bootstrap/Row";

const Share = (props) => {
  return (
    <Row className="mt-2 mb-2">
      <EmailShareButton
        className="mr-1"
        children={<EmailIcon size={32} round={true}></EmailIcon>}
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        subject="Check this awesome graffiti"
        body="I just discovered this website, web2020.netlify.app and it's amazing, I'm thinking about doing a tourist route of Graffitis, check this awesome graffiti."
      ></EmailShareButton>
      <FacebookShareButton
        className="mr-1"
        children={<FacebookIcon size={32} round={true}></FacebookIcon>}
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        quote="Check this awesome graffiti"
        hashtag="graffiti"
      ></FacebookShareButton>
      <LinkedinShareButton
        className="mr-1"
        children={<LinkedinIcon size={32} round={true}></LinkedinIcon>}
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        title="Check this awesome graffiti"
        description="I just discovered this website, web2020.netlify.app and it's amazing, I'm thinking about doing a tourist route of Graffitis, check this awesome graffiti."
        source="https://web-2020.netlify.app/"
      ></LinkedinShareButton>
      <PinterestShareButton
        className="mr-1"
        children={<PinterestIcon size={32} round={true}></PinterestIcon>}
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        description="Check this awesome graffiti"
      ></PinterestShareButton>
      <RedditShareButton
        className="mr-1"
        children={
          <RedditIcon
            size={32}
            round={true}
            bgStyle={{ fill: "#FF5700" }}
          ></RedditIcon>
        }
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        title="Check this awesome graffiti"
      ></RedditShareButton>
      <TelegramShareButton
        className="mr-1"
        children={<TelegramIcon size={32} round={true}></TelegramIcon>}
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        title="Check this awesome graffiti"
      ></TelegramShareButton>
      <TwitterShareButton
        className="mr-1"
        children={<TwitterIcon size={32} round={true}></TwitterIcon>}
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        title={"Check this awesome graffiti"}
        hashtags={["graffiti", "covid", "malaga"]}
      ></TwitterShareButton>
      <WhatsappShareButton
        className="mr-1"
        children={<WhatsappIcon size={32} round={true}></WhatsappIcon>}
        url={"https://web-2020.netlify.app/graffitis/" + props.id}
        title="Check this awesome graffiti"
      ></WhatsappShareButton>
    </Row>
  );
};

export default Share;
