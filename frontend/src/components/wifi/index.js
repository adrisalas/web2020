import React, { useEffect, useState } from "react";
import { Container, ToggleButton, ButtonGroup } from "react-bootstrap";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";
import Row from "react-bootstrap/Row";
import Col from "react-bootstrap/Col";

//mi malaguita
const DEFAULT_LAT = 36.711608;
const DEFAULT_LONG = -4.4323638;
const baseUrl = process.env.REACT_APP_BACKEND_URL + "/opendata"; //local

const parkColour = "#DF0101";
//const fontColour = '';
const wifiColour = "#000000";

const Wifi = () => {
  const [wifiPoints, setWifiPoints] = useState([]);
  const [parks, setParks] = useState([]);
  const [fonts, setFonts] = useState([]);
  const [selectedWifi, setSelectedWifi] = useState(false);
  const [selectedParks, setSelectedParks] = useState(false);
  const [selectedFonts, setSelectedFonts] = useState(false);

  const fillWifi = (data) => {
    let rdo = [];
    data.map((item) => {
      const { id, coordinates, location, finalidad } = item;
      if (
        coordinates !== undefined &&
        coordinates[0] !== undefined &&
        coordinates[1] !== undefined
      ) {
        rdo.push(item);
      }
    });
    setWifiPoints(rdo);
  };

  const fillParks = (data) => {
    let rdo = [];
    data.map((item) => {
      const { id, coordinates, direccion, nombre, info, titularidad } = item;
      if (
        coordinates !== undefined &&
        coordinates[0] !== undefined &&
        coordinates[1] !== undefined
      ) {
        rdo.push(item);
      }
    });
    setParks(rdo);
  };

  const fillFonts = (data) => {
    let rdo = [];
    data.map((item) => {
      const { id, coordinates, nombre } = item;
      if (
        coordinates !== undefined &&
        coordinates[0] !== undefined &&
        coordinates[1] !== undefined
      ) {
        rdo.push(item);
      }
    });
    setFonts(rdo);
  };

  const getData = async (item) => {
    const url = baseUrl + "/" + item;
    //const res = await fetch(url);
    console.log(url);
    const res = await fetch(url);
    const data = await res.json();
    console.log(data);
    let rdo;
    if (item === "wifi") {
      rdo = await fillWifi(data);
    } else if (item === "parques") {
      rdo = await fillParks(data);
    } else if (item === "fuentes") {
      rdo = await fillFonts(data);
    }
    console.log(rdo);
  };

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.checked;
    console.log(name + " " + value);
    if (name === "wifi") {
      setSelectedWifi(!selectedWifi);
      if (value && wifiPoints.length === 0) getData(name);
    } else if (name === "parques") {
      setSelectedParks(!selectedParks);
      if (value && parks.length === 0) getData(name);
    } else if (name === "fuentes") {
      setSelectedFonts(!selectedFonts);
      if (value && fonts.length === 0) getData(name);
    }
  };

  useEffect(() => {
    //getData("wifi");
    //getData("wifi");
  }, []);

  return (
    <Container>
      <Row>
        <h1>Public data points in Malaga</h1>
      </Row>
      <Row className="mt-2 mb-2">
        <Col>
          <ButtonGroup toggle className="mb-2">
            <ToggleButton
              type="checkbox"
              variant="outline-primary"
              checked={selectedWifi}
              name="wifi"
              onChange={handleChange}
            >
              Wifi Points
            </ToggleButton>
          </ButtonGroup>
        </Col>
        <Col>
          <ButtonGroup toggle className="mb-2">
            <ToggleButton
              type="checkbox"
              variant="outline-primary"
              checked={selectedFonts}
              name="fuentes"
              onChange={handleChange}
            >
              Fountains
            </ToggleButton>
          </ButtonGroup>
        </Col>
        <Col>
          <ButtonGroup toggle className="mb-2">
            <ToggleButton
              type="checkbox"
              variant="outline-primary"
              checked={selectedParks}
              name="parques"
              onChange={handleChange}
            >
              Dog parks
            </ToggleButton>
          </ButtonGroup>
        </Col>
      </Row>
      <Row>
        <MapContainer
          style={{ marginBottom: "5rem" }}
          id="wifiMap"
          center={[DEFAULT_LAT, DEFAULT_LONG]}
          zoom={13}
          scrollWheelZoom={true}
        >
          <TileLayer
            attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
          {selectedWifi
            ? wifiPoints.map((wifiPoint) => {
                const { id, coordinates, location, finalidad } = wifiPoint;
                return (
                  <Marker position={[coordinates[1], coordinates[0]]}>
                    <Popup>
                      {id} <br />
                      {location}
                    </Popup>
                  </Marker>
                );
              })
            : ""}
          {selectedFonts
            ? fonts.map((f) => {
                const { id, coordinates, nombre } = f;
                return (
                  <Marker position={[coordinates[1], coordinates[0]]}>
                    <Popup style="color: #DF0101">
                      {id} <br />
                      {nombre}
                    </Popup>
                  </Marker>
                );
              })
            : ""}
          {selectedParks
            ? parks.map((p) => {
                const {
                  id,
                  coordinates,
                  direccion,
                  nombre,
                  info,
                  titularidad,
                } = p;
                return (
                  <Marker position={[coordinates[1], coordinates[0]]}>
                    <Popup style="color:#DF0101;">
                      <style></style>
                      {id} <br />
                      {nombre} <br />
                      Dirección: {direccion} <br />
                      {titularidad} <br />
                      Info extra: {info}
                    </Popup>
                  </Marker>
                );
              })
            : ""}
        </MapContainer>
      </Row>
    </Container>
  );
};

export default Wifi;
