import React, { useState, useEffect } from "react";
import { Container, Row, Col, Table } from "react-bootstrap";
import { MapContainer, Marker, Popup, TileLayer } from "react-leaflet";

const GraffitisMap = (props) => {
  const [center, setCenter] = useState([]);
  const [graffitis, setGraffitis] = useState([]);

  useEffect(() => {
    setGraffitis(props.listado);
    console.log(props);
    setCenter([props.lat || 36.721261, props.lon || -4.4212655]);
  }, []);

  const selectLocation = (gra) => {
    console.log(gra);
    setCenter([gra.location.lat, gra.location.lon]);
  };

  const table = graffitis.map((gra) => {
    return (
      <tr onClick={() => selectLocation(gra)}>
        <td>{gra?.title}</td>
        <td>{gra.author?.name}</td>
        <td>{new Date(gra.date).toLocaleDateString("es-ES")}</td>
      </tr>
    );
  });

  const map = graffitis.map((gra) => {
    return (
      <Marker key={gra?._id} position={[gra?.location.lat, gra?.location.lon]}>
        <Popup>
          {gra.title}
          <br />
          <img
            style={{ maxWidth: "10rem", maxHeight: "10rem" }}
            src={gra?.image}
            alt={gra?.title}
          />
        </Popup>
      </Marker>
    );
  });

  return (
    <Container style={{ marginBottom: "5rem" }}>
      <Row>
        <Col>
          <h1>Graffitis</h1>
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Title</th>
                <th>Author</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody>{graffitis[0] ? table : "  "}</tbody>
          </Table>
        </Col>
        <Col>
          <h1>Map</h1>
          <MapContainer
            id="graffitimap"
            key={center.length > 0 ? center : [36.721261, -4.4212655]}
            center={center.length > 0 ? center : [36.721261, -4.4212655]}
            zoom={13}
            scrollWheelZoom={true}
          >
            <TileLayer
              attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            />
            {map}
          </MapContainer>
        </Col>
      </Row>
    </Container>
  );
};

export default GraffitisMap;
