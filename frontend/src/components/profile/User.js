import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Container, Button, Form } from "react-bootstrap";
import messageIcon from "../../assets/images/message.jpg";
import defaultUser from "../../assets/images/defaultUser.jpg";
import defaultBanner from "../../assets/images/defaultBanner.jpg";
import "./profile.css";
import uploadImageProfile from "../commons/uploadImageProfile";
import uploadBannerProfile from "../commons/uploadBannerProfile";

const url = process.env.REACT_APP_BACKEND_URL + "/users/";

export default function User() {
  const [userSeleccionado, setUserSelected] = useState(
    JSON.parse(sessionStorage.getItem("user"))
  );

  useEffect(() => {
    var myHeaders = new Headers();
    myHeaders.append("Content-Type", "application/json");

    var requestOptions = {
      method: "GET",
      headers: myHeaders,
      redirect: "follow",
    };

    fetch("http://localhost:3030/users/" + userSeleccionado._id, requestOptions)
      .then((result) => result.json())
      .then((data) => setUserSelected(data))
      .catch((error) => console.log("error", error));
  }, []);

  const selectUser = (elemento, caso) => {
    setUserSelected(elemento);
  };
  /* 
    const removeUser = (id) => {
        fetch(url + id, {
            method: "DELETE",
        });
    }; */

  const handleChange = (e) => {
    console.log(e);
    console.log(e.target.value);
    console.log(userSeleccionado);
    const { name, value } = e.target;
    setUserSelected((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const editar = (e) => {
    e.preventDefault();
    updateUser(userSeleccionado);
  };

  const updateUser = async (user) => {
    console.log(JSON.stringify(user));
    await fetch(url + user._id, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify(user),
      redirect: "follow",
    })
      .then(() => {
        sessionStorage.setItem("user", JSON.stringify(user));
        window.location.reload(false);
        alert("User edit successfully!!")
      })
      .catch((err) => {
        alert(err);
      });
    console.log();
  };

  return (
    <Container>
      <div class="row row-cols-2">
        <div class="col-3">
          <img
            style={{ maxHeight: "10rem", maxWidth: "10rem" }}
            alt="profile"
            src={userSeleccionado.image.split("=")[0] || defaultUser}
          />
        </div>
        {userSeleccionado.rol === "admin" ? ( <div class="col-7">
          <img
            style={{ maxHeight: "10rem", maxWidth: "60rem" }}
            alt="banner"
            src={userSeleccionado.banner || defaultBanner}
          />
        </div>) : ( <div class="col-9">
          <img
            style={{ maxHeight: "10rem", maxWidth: "60rem" }}
            alt="banner"
            src={userSeleccionado.banner || defaultBanner}
          />
        </div>    
        )}
        {userSeleccionado.rol == "admin" ? (<div class="col-2">
        <Button
            as={Link}
            operation="read"
            to={`/admin`}
            variant="warning"
          >
            Admin Panel
          </Button>
        </div>) : (<></>)}
       
        
      </div>

      <br />

      <Form as="form" onSubmit={editar}>
        <Form.Group controlId="name">
          <Form.Label> Name </Form.Label>
          <Form.Control
            className="form-control"
            required
            type="text"
            name="name"
            value={userSeleccionado && userSeleccionado.name}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Control.Feedback type="invalid">
          Please insert a name.
        </Form.Control.Feedback>
        <Form.Group controlId="description">
          <Form.Label> Description </Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            name="description"
            value={userSeleccionado && userSeleccionado.description}
            onChange={handleChange}
          />
        </Form.Group>
        <Form.Group controlId="image">
          <Form.Label> Image </Form.Label>
          <br />
          <Form.Control
            type="file"
            name="image"
            className="input-image"
            onChange={() =>
              uploadImageProfile({ userSeleccionado, setUserSelected })
            }
          />
        </Form.Group>

        <Form.Group controlId="banner">
          <Form.Label> Banner </Form.Label>
          <Form.Control
            type="file"
            name="banner"
            className="input-banner"
            onChange={() =>
              uploadBannerProfile({ userSeleccionado, setUserSelected })
            }
          />
        </Form.Group>
        <Form.Group controlId="rol">
          <Form.Label> Rol </Form.Label>
          <Form.Control
            required
            type="text"
            readOnly="true"
            name="rol"
            value={userSeleccionado && userSeleccionado.rol}
          />
        </Form.Group>

        <Form.Group controlId="experience">
          <Form.Label> Experience </Form.Label>
          <Form.Control
            required
            type="text"
            readOnly="true"
            name="experience"
            value={userSeleccionado && userSeleccionado.experience}
          />
        </Form.Group>

        <button className="btn btn-primary btn-block" onClick={editar}>
          Edit
        </button>
        <br />
        <br />
      </Form>
    </Container>
  );
}
