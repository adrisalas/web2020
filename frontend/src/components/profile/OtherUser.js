import React, { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import { Container, Button, Form } from "react-bootstrap";
import { Redirect, useHistory } from "react-router";
import messageIcon from "../../assets/images/message.jpg";
import defaultUser from "../../assets/images/defaultUser.jpg";
import defaultBanner from "../../assets/images/defaultBanner.jpg";
import "./profile.css";

const url = process.env.REACT_APP_BACKEND_URL + "/users/";

export default function OtherUser() {
  const [userSeleccionado, setUserSelected] = useState([]);
  const { _id } = useParams();
  const history = useHistory();
  const selectUser = (elemento) => {
    setUserSelected(elemento);
  };

  const user = JSON.parse(sessionStorage.getItem("user"));

  useEffect(() => {
    fetch(url + _id)
      .then((response) => response.json())
      .then((user) => {
        setUserSelected(user);
      });
  }, []);

  const envelopeClose = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="64"
      height="64"
      fill="#007bff"
      className="bi bi-envelope"
      viewBox="0 0 16 16"
    >
      <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2zm13 2.383l-4.758 2.855L15 11.114v-5.73zm-.034 6.878L9.271 8.82 8 9.583 6.728 8.82l-5.694 3.44A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.739zM1 11.114l4.758-2.876L1 5.383v5.73z" />
    </svg>
  );

  const envelopeOpen = (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      width="64"
      height="64"
      fill="#007bff"
      class="bi bi-envelope-open"
      viewBox="0 0 16 16"
    >
      <path d="M8.47 1.318a1 1 0 0 0-.94 0l-6 3.2A1 1 0 0 0 1 5.4v.818l5.724 3.465L8 8.917l1.276.766L15 6.218V5.4a1 1 0 0 0-.53-.882l-6-3.2zM15 7.388l-4.754 2.877L15 13.117v-5.73zm-.035 6.874L8 10.083l-6.965 4.18A1 1 0 0 0 2 15h12a1 1 0 0 0 .965-.738zM1 13.117l4.754-2.852L1 7.387v5.73zM7.059.435a2 2 0 0 1 1.882 0l6 3.2A2 2 0 0 1 16 5.4V14a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V5.4a2 2 0 0 1 1.059-1.765l6-3.2z" />
    </svg>
  );

  const [isEnvelopeOpen, setIsEnvelopeOpen] = useState(false);
  return (
    <Container>
      <div class="row row-cols-2">
        <div class="col-3">
          <img
            height="100%"
            alt="profile"
            width="100%"
            src={
              (userSeleccionado &&
                userSeleccionado.image &&
                userSeleccionado.image.split("=")[0]) ||
              defaultUser
            }
          />
        </div>
        <div class="col-7">
          <img
            alt="banner"
            height="100%"
            width="100%"
            src={(userSeleccionado && userSeleccionado.banner) || defaultBanner}
          />
        </div>
        <div class="col-2">
          {user && user._id !== userSeleccionado._id ? (
            <i
              onMouseEnter={() => setIsEnvelopeOpen(true)}
              onMouseLeave={() => setIsEnvelopeOpen(false)}
              style={{
                cursor: "pointer",
                color: "primary",
                color: "@primary",
              }}
              onClick={() => {
                history.push("/conversations/" + user._id + "/" + _id);
              }}
            >
              {isEnvelopeOpen ? envelopeOpen : envelopeClose}
            </i>
          ) : undefined}
        </div>
      </div>

      <br />

      <Form>
        <Form.Group controlId="name">
          <Form.Label> Name </Form.Label>
          <Form.Control
            className="form-control"
            type="text"
            readOnly="true"
            name="name"
            value={userSeleccionado && userSeleccionado.name}
          />
        </Form.Group>
        <Form.Group controlId="description">
          <Form.Label> Description </Form.Label>
          <Form.Control
            as="textarea"
            rows={3}
            readOnly="true"
            name="description"
            value={userSeleccionado && userSeleccionado.description}
          />
        </Form.Group>

        <Form.Group controlId="rol">
          <Form.Label> Rol </Form.Label>
          <Form.Control
            required
            type="text"
            readOnly="true"
            name="rol"
            value={userSeleccionado && userSeleccionado.rol}
          />
        </Form.Group>

        <Form.Group controlId="experience">
          <Form.Label> Experience </Form.Label>
          <Form.Control
            required
            type="text"
            readOnly="true"
            name="experience"
            value={userSeleccionado && userSeleccionado.experience}
          />
        </Form.Group>
      </Form>
    </Container>
  );
}
