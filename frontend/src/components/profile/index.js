import React from "react";
import "react-dropdown/style.css";
import "./profile.css";
import { Redirect } from 'react-router'
import User from './User';

const UserList = () => {

  const user = JSON.parse(sessionStorage.getItem("user"));

  let pagina = user ? (<User></User>) : (<Redirect to="/" />);

  return pagina;
};

export default UserList;
