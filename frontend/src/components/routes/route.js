import { useParams } from "react-router-dom";
import React from "react";
import { useFetch } from "../../hooks/useFetch";
import Error from "../error";
import { Form } from "react-bootstrap";
import Comments from "../comment/Comments";
import GraffitisMap from "../commons/GraffitisMap";
import { Redirect } from "react-router-dom";
import { Tab, Tabs, Row, Col } from "react-bootstrap";
import Votes from "../comment/Votes";

const inicio = process.env.REACT_APP_BACKEND_URL;
const inicioUrl = inicio + "/routes/";

let usuario = undefined;
let user = {};

const requestOptions = {
  method: "GET",
  redirect: "follow",
};

const Route = () => {
  const { id } = useParams();
  const url = inicioUrl + id;
  const { isLoading, data, isError } = useFetch({ url, requestOptions });
  usuario = sessionStorage.getItem("user");
  if (usuario) {
    user = JSON.parse(sessionStorage.getItem("user"));
  } else {
    return <Redirect to="/" />;
  }

  if (isError) {
    return (
      <>
        <Error />
      </>
    );
  }

  return (
    <>
      <h1
        style={{ display: "flex", justifyContent: "center", marginTop: "20px" }}
      >
        {data.title}
      </h1>
      {data.votes && user._id ? (
                <Row className="d-flex justify-content-center">
                  <Votes
                    key={data.votes}
                    userId={user._id}
                    votes={data.votes}
                    update={(v) => {
                      data.votes = v;
                    }}
                    id={data._id}
                    type="routes"
                    displayVotes={true}
                  />
                </Row>
              ) : undefined}
      <Tabs defaultActiveKey="listado" mountOnEnter={true} id="uncontrolled-tab-example">
        <Tab eventKey="listado" title="Info">
          {/* Detalles de la ruta */}
          <div>
            <h3
              style={{
                display: "flex",
                justifyContent: "center",
                marginTop: "1%",
              }}
            >
              {!isLoading && "Information"}
            </h3>
            <h2 style={{ display: "flex", justifyContent: "center" }}>
              {isLoading && "loading..."}
            </h2>
            <Form
              as="form"
              style={{ width: "80%", marginLeft: "10%", marginRight: "10%" }}
            >
              <Form.Group controlId="title">
                <Form.Label>Title </Form.Label>
                <Form.Control
                  type="text"
                  name="title"
                  maxLength="30"
                  value={data.title}
                  required
                  disabled={true}
                />
              </Form.Group>
              <Form.Group controlId="description">
                <Form.Label>Description </Form.Label>
                <Form.Control
                  as="textarea"
                  rows={3}
                  name="description"
                  required
                  maxLength="500"
                  value={data.description}
                  disabled={true}
                />
              </Form.Group>

              <Form.Group controlId="city">
                <Form.Label>City </Form.Label>
                <Form.Control
                  type="text"
                  name="city"
                  required
                  maxLength="30"
                  value={data.city}
                  disabled={true}
                />
              </Form.Group>
              <Form.Group controlId="selectedGraffitis">
                <Form.Label>Graffitis</Form.Label>
                <br />
                <Form.Control
                  name="selectedGraffitis"
                  as="select"
                  htmlSize={6}
                  disabled={true}
                  custom
                >
                  {data.graffitis &&
                    data.graffitis.map((graffiti) => {
                      return (
                        <option
                          key={graffiti.graffiti_id}
                          value={graffiti.graffiti_id}
                        >
                          {graffiti.title}
                        </option>
                      );
                    })}
                </Form.Control>
              </Form.Group>
                Author:
                <a href={"/profile/" + data?.author?.author_id}>
                    {data?.author?.name}
                </a>
            </Form>
          </div>
        </Tab>
        <Tab eventKey="mapa" title="Map">
          {/* Mapa */}
          {!isLoading && (
            <GraffitisMap
              lat={data?.location?.lat}
              lon={data?.location?.lon}
              listado={data?.graffitis}
            />
          )}
        </Tab>
      </Tabs>
      <hr style={{ marginLeft: "10%", marginRight: "10%", marginTop: "1%" }} />
      <h3
        style={{ display: "flex", justifyContent: "center", marginTop: "1%" }}
      >
        Comments
      </h3>
      <Comments
        userName={user?.name}
        userId={user?._id}
        type="Route"
        typeId={id}
      />
      <br />
    </>
  );
};

export default Route;
