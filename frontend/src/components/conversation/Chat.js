import React, { useState, useEffect } from "react";
import "./Chat.css";
import Button from "react-bootstrap/Button";
import { ListGroup, Container, Row, Col, Form } from "react-bootstrap";
import { useParams } from "react-router-dom";
const remotebaseUrl = process.env.REACT_APP_BACKEND_URL + "/conversations"; //remote
const localbaseUrl = process.env.REACT_APP_BACKEND_URL + "/conversations"; //local
//const userbaseUrl = process.env.REACT_APP_BACKEND_URL + "/conversations"; //remote
const userbaseUrl = process.env.REACT_APP_BACKEND_URL + "/users/"; //local
let usuario = undefined;
let user = {};

const Chat = () => {
  //const {id} = useParams();
  //const url = remotbaseUrl+"/"+id;
  const { user1, user2 } = useParams();
  const url = localbaseUrl + "?user1=" + user1 + "&user2=" + user2;
  console.log(url);
  const [input, setInput] = useState("");
  const [id, setid] = useState("");
  const [chat, setChat] = useState({
    users: [
      {
        _id: "",
        nickname: "",
      },
      {
        _id: "",
        nickname: "",
      },
    ],
    messages: [],
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(input);
    if (input) {
      sendMessage();
    }
  };

  const createChat = async (u2) => {
    let url = userbaseUrl + u2;
    console.log(url);
    let dest;
    //get dest user
    await fetch(url)
      .then((res) => res.json())
      .then((data) => {
        dest = data;
        //console.log(dest);
      })
      .catch((e) => console.log(e));
    url = userbaseUrl + user1;

    console.log("CREAR");
    console.log("SRC: " + user._id + " " + user.name);
    console.log("DEST: " + dest._id + " " + dest.name);
    await fetch(localbaseUrl, {
      method: "POST",
      body: JSON.stringify({
        users: [
          {
            _id: user._id,
            nickname: user.name,
          },
          {
            _id: dest._id,
            nickname: dest.name,
          },
        ],
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
      redirect: "follow",
    })
      .then((res) => console.log(res))
      .then(getChat);
    //getChat();
  };

  const getChat = async () => {
    console.log(url);
    fetch(url)
      .then((res) => res.json())
      .then((chats) => {
        console.log(chats[0]);
        if (chats && chats.length === 0) {
          createChat(user2);
        } else {
          setChat(chats[0]);
          setid(chats[0]._id);
        }
      })
      .catch((e) => console.log(e));
  };

  function getTimeFromDate(date) {
    let displayDifference = "";
    let timeDifference = new Date().getTime() - new Date(date).getTime();
    if (timeDifference <= 120000) {
      displayDifference = "less than 1 min ago";
    } else if (timeDifference <= 3.6e6) {
      displayDifference = parseInt(timeDifference / 60000) + " mins ago";
    } else if (timeDifference <= 7.2e6) {
      displayDifference = " 1 hour ago";
    } else if (timeDifference <= 8.64e7) {
      displayDifference = parseInt(timeDifference / 3600000) + " hours ago";
    } else if (timeDifference <= 1.728e8) {
      displayDifference = "1 day ago";
    } else {
      displayDifference = parseInt(timeDifference / 86400000) + " days ago";
    }
    return displayDifference;
  }

  useEffect(() => {
    usuario = sessionStorage.getItem("user");
    console.log("usuario ");
    if (user) {
      user = JSON.parse(sessionStorage.getItem("user"));
      getChat();
    }
  }, []);

  //MANDAR MENSAJE
  const sendMessage = async () => {
    const sendUrl = localbaseUrl + "/" + id + "/send";
    console.log(sendUrl);
    await fetch(sendUrl, {
      method: "POST",
      body: JSON.stringify({
        message: input,
        user: user._id,
      }),
      headers: {
        "Content-type": "application/json",
      },
      redirect: "follow",
    });
    setInput("");
    getChat();
  };

  return (
    <>
      <Container>
        <Row></Row>
        <Row>
          <Col>
            <h4>
              Conversation between
              <br />
              {chat.users[0].nickname} and {chat.users[1].nickname}
            </h4>
          </Col>
        </Row>
        <Row>
          {chat.messages.length === 0 ? (
            <h3>There are no messages yet... say something nice!</h3>
          ) : (
            <Container>
              <ListGroup>
                {chat.messages.map((message) => {
                  return (
                    <Row className="mb-2">
                      {message.user == user._id ? (
                        <Col xs={4}>
                          <ListGroup.Item
                            style={{ backgroundColor: "#d6eaf8" }}
                          >
                            <div class="d-flex justify-content-between">
                              <p>
                                By{" "}
                                <a
                                  href={"/profile/"}
                                  style={{ color: "inherit" }}
                                >
                                  {user.name}
                                </a>{" "}
                              </p>
                              <small>{getTimeFromDate(message.date)}</small>
                            </div>
                            {message.message}
                          </ListGroup.Item>
                        </Col>
                      ) : (
                        <Col xs={{ span: 4, offset: 3 }}>
                          <ListGroup.Item
                            style={{ backgroundColor: "#ececec" }}
                          >
                            <div class="d-flex justify-content-between">
                              <p>
                                By{" "}
                                {chat.users[0].nickname !== user.name ? (
                                  <a
                                    href={"/profile/" + chat.users[0]._id}
                                    style={{ color: "inherit" }}
                                  >
                                    {chat.users[0].nickname}
                                  </a>
                                ) : (
                                  <a
                                    href={"/profile/" + chat.users[1]._id}
                                    style={{ color: "inherit" }}
                                  >
                                    {chat.users[1].nickname}
                                  </a>
                                )}
                              </p>
                              <small>{getTimeFromDate(message.date)}</small>
                            </div>
                            {message.message}
                          </ListGroup.Item>
                        </Col>
                      )}
                    </Row>
                  );
                })}
              </ListGroup>
            </Container>
          )}
        </Row>

        <Row>
          <Form>
            <Form.Group>
              <Form.Control
                name="input"
                type="text"
                value={input}
                placeholder="write something nice"
                onChange={(e) => {
                  setInput(e.target.value);
                }}
              />
            </Form.Group>
            <Button variant="info" onClick={handleSubmit} type="submit">
              Send
            </Button>
          </Form>
        </Row>
      </Container>
      )
    </>
  );
};

export default Chat;
