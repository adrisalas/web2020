import React, { useEffect, useState } from "react";
import Chat from "./Chat";

import { ListGroup, Button, Form, Container, Row, Col } from "react-bootstrap";
//import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import {Link, Redirect, useHistory} from "react-router-dom";

const remotebaseUrl = process.env.REACT_APP_BACKEND_URL + "/conversations"; //remote
const localbaseUrl =
  process.env.REACT_APP_BACKEND_URL + "/conversations?userId="; //local
let usuario= undefined;
let user={};
const Conversations = () => {
  const [conversations, setConversations] = useState([]);
  const [conversation, setConversation] = useState({
    user0Id: "",
    user0nickname: "",
    user1Id: "",
    user1nickname: "",
  });

  const removeConversation = async (id) => {
    const url = remotebaseUrl + "/" + id;
    await fetch(url, { method: "DELETE" });
    getConversations();
  };

  const handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setConversation({ ...conversation, [name]: value });
    //console.log(conversation);
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (
      conversation.user0nickname &&
      conversation.user0nickname &&
      conversation.user1Id &&
      conversation.user1nickname
    ) {
      createConversation();
    }
  };

  const createConversation = async () => {
    const url = remotebaseUrl;
    console.log(conversation);
    await fetch(url, {
      method: "POST",
      body: JSON.stringify({
        users: [
          {
            _id: conversation.user0Id,
            nickname: conversation.user0nickname,
          },
          {
            _id: conversation.user1Id,
            nickname: conversation.user1nickname,
          },
        ],
      }),
      headers: {
        "Content-type": "application/json; charset=UTF-8",
      },
      redirect: "follow",
    });
    setConversation({
      user0Id: "",
      user0nickname: "",
      user1Id: "",
      user1nickname: "",
    });
    getConversations();
  };

  const getConversations = async () => {
    try {
      console.log(user);
      const url = localbaseUrl + user._id;
      console.log(url);
      const res = await fetch(url);
      const conversations = await res.json();
      console.log(conversations);
      setConversations(conversations);
    } catch (e) {
      console.log(e);
    }
  };

  const history = useHistory();
  useEffect(() => {
    usuario=sessionStorage.getItem("user");
    console.log(usuario);
    if(usuario){
      user=JSON.parse(sessionStorage.getItem("user"));
      getConversations(); //fetch data
    }else{
      history.push("/");
    }
  }, []);

  return (
    <>
      {usuario?
          <Container>
            <Row>
              <Col>
                <Row>
                  <h3>Conversations</h3>
                </Row>
                <Row>
                  <ListGroup>
                    {conversations && conversations.length > 0?
                        conversations.map((conversation) => {
                      const { _id, last_updated, users, messages } = conversation;
                      return (
                          <ListGroup.Item key={_id}>
                            <p>Last view: {last_updated}</p><br/>
                            <p>{messages.length === 0? "No messages" : messages.length + " messages"}</p><br/>
                            <p>{users[0].nickname} and {users[1].nickname}</p>
                            <Button href={`/conversations/${users[0]._id}/${users[1]._id}`} variant={"primary"}>
                              Chat
                            </Button>
                          </ListGroup.Item>
                      );
                    })
                    :
                    <p>There are no conversations to show. To start a conversation, go to another user's profile and click "chat".</p>
                    }
                  </ListGroup>
                </Row>
              </Col>
            </Row>
          </Container>
          :
          <p>Loading</p>
      }
    </>
  );
};

export default Conversations;

/*<Col>
            <Row>
              <h1>Create a conversation</h1>
            </Row>
            <Row>
              <Form>
                <Row>
                  <Col>
                    <Form.Group controlId="firstUser">
                      <Form.Label>First user id</Form.Label>
                      <Form.Control
                        name="user0Id"
                        type="text"
                        value={conversation.user0Id}
                        onChange={handleChange}
                      />
                      <Form.Label>First user nickname</Form.Label>
                      <Form.Control
                        name="user0nickname"
                        type="text"
                        value={conversation.user0nickname}
                        onChange={handleChange}
                      />
                    </Form.Group>
                  </Col>
                  <Col>
                    <Form.Group controlId="secondUser">
                      <Form.Label>Second user id</Form.Label>
                      <Form.Control
                        name="user1Id"
                        type="text"
                        value={conversation.user1Id}
                        onChange={handleChange}
                      />
                      <Form.Label>Second user nickname</Form.Label>
                      <Form.Control
                        name="user1nickname"
                        type="text"
                        value={conversation.user1nickname}
                        onChange={handleChange}
                      />
                    </Form.Group>
                  </Col>
                </Row>
                <Row>
                  <Button
                    variant="primary"
                    type="submit"
                    onClick={handleSubmit}
                  >
                    Create
                  </Button>
                </Row>
              </Form>
            </Row>
          </Col>*/

/*<Button variant="danger" onClick={() => removeConversation(_id)}>
                        Delete
  </Button>*/
