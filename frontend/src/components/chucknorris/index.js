import React, { useState, useEffect } from "react";
import Toast from "react-bootstrap/Toast";
import Spinner from "react-bootstrap/Spinner";

const ChuckNorris = () => {
  const [quote, setQuote] = useState("");
  const [showChuckNorris, setShowChuckNorris] = useState(true);

  function getNewQuote() {
    setQuote("");
    fetch("https://api.chucknorris.io/jokes/random?category=dev")
      .then((data) => data.json())
      .then((data) => setQuote(data.value))
      .catch((err) => console.log(err));
  }

  useEffect(() => {
    getNewQuote();
  }, []);

  return (
    <Toast
      style={{
        position: "fixed",
        bottom: 10,
        right: 10,
        zIndex: 999,
      }}
      show={showChuckNorris}
      onClose={() => {
        setShowChuckNorris(false);
      }}
    >
      <Toast.Header>
        <img
          src={process.env.PUBLIC_URL + "/chuck.png"}
          className="rounded"
          alt=""
        />
        <strong className="mr-auto ml-2">api.chucknorris.io</strong>
        <strong>
          <i onClick={() => getNewQuote()} style={{ cursor: "pointer" }}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="16"
              height="16"
              fill="currentColor"
              className="bi bi-arrow-counterclockwise"
              viewBox="0 0 16 16"
            >
              <path
                fillRule="evenodd"
                d="M8 3a5 5 0 1 1-4.546 2.914.5.5 0 0 0-.908-.417A6 6 0 1 0 8 2v1z"
              />
              <path d="M8 4.466V.534a.25.25 0 0 0-.41-.192L5.23 2.308a.25.25 0 0 0 0 .384l2.36 1.966A.25.25 0 0 0 8 4.466z" />
            </svg>
          </i>
        </strong>
      </Toast.Header>
      <Toast.Body>
        {quote === "" ? (
          <Spinner
            style={{ left: "50%", right: "50%", position: "relative" }}
            animation="border"
            role="status"
          >
            <span className="sr-only">Loading...</span>
          </Spinner>
        ) : (
          <>{quote}</>
        )}
      </Toast.Body>
    </Toast>
  );
};

export default ChuckNorris;
